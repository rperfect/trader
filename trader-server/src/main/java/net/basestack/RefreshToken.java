package net.basestack;

import net.troja.eve.esi.ApiClient;
import net.troja.eve.esi.ApiClientBuilder;
import net.troja.eve.esi.auth.OAuth;
import net.troja.eve.esi.auth.SsoScopes;

import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RefreshToken {

    public static final String CLIENT_ID = "c70f987c21804b79b675303efb3e7d4a";


    public static void main(String[] args) throws Exception {

        final String state = "somesecret";
        ApiClient client = new ApiClientBuilder().clientID(CLIENT_ID).build();

        final OAuth auth = (OAuth) client.getAuthentication("evesso");
        final Set<String> scopes = new HashSet<>(Arrays.asList(
                SsoScopes.ESI_MARKETS_READ_CHARACTER_ORDERS_V1
        ));

        String redirectUri = "http://localhost/oauth-callback";

        final String authorizationUri = auth.getAuthorizationUri(redirectUri, scopes, state);
        System.out.println("Authorization URL: " + authorizationUri);

        Desktop.getDesktop().browse(new URI(authorizationUri));

        final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Code from Answer: ");
        final String code = br.readLine();
        auth.finishFlow(code, state);
        System.out.println("Refresh Token: " + auth.getRefreshToken());

    }
}
