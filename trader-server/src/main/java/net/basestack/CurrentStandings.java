package net.basestack;

import net.evetrader.EveTraderApplication;
import net.troja.eve.esi.ApiClient;
import net.troja.eve.esi.ApiClientBuilder;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.api.CharacterApi;
import net.troja.eve.esi.model.CharacterStandingsResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CurrentStandings {

    public static final String CLIENT_ID = "ef2782230f69486c92dedf7b5527f66c";
    public static final String REFRESH_TOKEN = "_gtSXtpVJTox6EWJ9neFPKQsaa4atQfoBA2t4BdXz2E";

    protected static final String DATASOURCE = "tranquility";

    private void checkStandings() throws ApiException {
        ApiClient client = new ApiClientBuilder().clientID(CLIENT_ID).refreshToken(REFRESH_TOKEN).build();

        CharacterApi api = new CharacterApi();
        api.setApiClient(client);

        List<CharacterStandingsResponse> responseList = api.getCharactersCharacterIdStandings(2115047450, DATASOURCE, null, null);
        System.out.println(responseList);
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);
        CurrentStandings currentStandings = applicationContext.getBean(CurrentStandings.class);
        currentStandings.checkStandings();
        System.exit(0);
    }

}
