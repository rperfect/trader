package net.evetrader.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 */
@Controller
public class DefaultViewController {

    @RequestMapping({
            "/app/**",
    })
    public String index() {
        return "forward:/";
    }
}

