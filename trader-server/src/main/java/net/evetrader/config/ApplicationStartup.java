package net.evetrader.config;

import net.evetrader.services.map.MapService;
import net.evetrader.services.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class ApplicationStartup implements ApplicationListener<ApplicationReadyEvent> {

    @Autowired
    protected ProductService productService;

    @Autowired
    protected MapService mapService;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {

        /*new Thread(() -> {
            System.out.println("Startup: Loading products - started");
            // Pump the product map to load now, for easier debugging later
            productService.getProductMap().getMap().get(123);

            System.out.println("Startup: Loading products - finished" + new Date());
        }).start();


        new Thread(() -> {
            System.out.println("Startup: Loading map - started");
            mapService.getGraphMap();
            System.out.println("Startup: Loading map - finished " + new Date());
        }).start();*/


    }
}
