package net.evetrader;

import net.troja.eve.esi.ApiClient;
import net.troja.eve.esi.ApiClientBuilder;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.api.MarketApi;
import net.troja.eve.esi.api.UniverseApi;
import net.troja.eve.esi.model.MarketOrdersResponse;
import net.troja.eve.esi.model.UniverseNamesResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MarketClient {

    public static final String CLIENT_ID = "ef2782230f69486c92dedf7b5527f66c";
    public static final String REFRESH_TOKEN = "_gtSXtpVJTox6EWJ9neFPKQsaa4atQfoBA2t4BdXz2E";


    public static final int domainRegionId = 10000043;
    public static final int theCitadelRegionId = 10000033;

    private ApiClient client;
    private UniverseApi universeApi;
    private MarketApi marketApi;

    public ApiClient getClient() {
        if (client == null) {
            client = new ApiClientBuilder().clientID(CLIENT_ID).refreshToken(REFRESH_TOKEN).build();
        }
        return client;
    }

    public UniverseApi getUniverseApi() {
        if (universeApi == null) {
            universeApi = new UniverseApi(getClient());
        }
        return universeApi;
    }

    public MarketApi getMarketApi() {
        if (marketApi == null) {
            marketApi = new MarketApi(getClient());
        }
        return marketApi;
    }

    public void getRegions() throws ApiException {
        List<Integer> universeRegions = getUniverseApi().getUniverseRegions(null, null);
        System.out.println("Region IDs: " + universeRegions);

        List<UniverseNamesResponse> universeNamesResponses = getUniverseApi().postUniverseNames(universeRegions, null);
        System.out.println("Names Response: " + universeNamesResponses);
    }

    public void getMarketData() throws ApiException {
        List<MarketOrdersResponse> ordersResponseList = getMarketApi().getMarketsRegionIdOrders("sell", domainRegionId, null, null, 1, null);
        Map<Integer, String> systemMap = getSystemMap(ordersResponseList);
        Map<Integer, String> locationMap = getLocationMap(ordersResponseList);
        Map<Integer, String> typeMap = getTypeMap(ordersResponseList);

        for(MarketOrdersResponse nextOrder : ordersResponseList) {
            System.out.printf("%-12s  ", systemMap.get(nextOrder.getSystemId()));
            System.out.printf("%-50s  ", typeMap.get(nextOrder.getTypeId()));
            System.out.printf("%10d  ", nextOrder.getVolumeRemain());
            System.out.printf("%10.2f  ", nextOrder.getPrice());
            System.out.printf("%-40s  ", locationMap.get(nextOrder.getLocationId().intValue()));

            System.out.println();
        }
        //System.out.println("Orders: " + ordersResponseList);
    }

    private Map<Integer, String> getSystemMap(List<MarketOrdersResponse> ordersResponseList) throws ApiException {
        Set<Integer> systemIdSet = new HashSet<>();
        for(MarketOrdersResponse nextOrder : ordersResponseList) {
            systemIdSet.add(nextOrder.getSystemId());
        }
        return getNameMap(systemIdSet);
    }

    private Map<Integer, String> getLocationMap(List<MarketOrdersResponse> ordersResponseList) throws ApiException {
        Set<Integer> locationIdSet = new HashSet<>();
        for(MarketOrdersResponse nextOrder : ordersResponseList) {
            locationIdSet.add(nextOrder.getLocationId().intValue());
        }
        return getNameMap(locationIdSet);
    }

    private Map<Integer, String> getTypeMap(List<MarketOrdersResponse> ordersResponseList) throws ApiException {
        Set<Integer> typeIdSet = new HashSet<>();
        for(MarketOrdersResponse nextOrder : ordersResponseList) {
            typeIdSet.add(nextOrder.getTypeId());
        }

        return getNameMap(typeIdSet);
    }

    private Map<Integer, String> getNameMap(Set<Integer> idSet) throws ApiException {
        List<Integer> idList = new ArrayList<>(idSet);

        List<UniverseNamesResponse> universeNamesResponses = getUniverseApi().postUniverseNames(idList, null);
        Map<Integer, String> nameMap = new HashMap<>();
        for(UniverseNamesResponse nextName : universeNamesResponses) {
            nameMap.put(nextName.getId(), nextName.getName());
        }
        return nameMap;
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);
        MarketClient marketClient = applicationContext.getBean(MarketClient.class);
        //marketClient.getRegions();
        marketClient.getMarketData();
        System.exit(0);
    }
}
