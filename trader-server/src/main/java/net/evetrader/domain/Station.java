package net.evetrader.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Station {

    private Integer regionID;
    private Integer solarSystemID;
    private Integer stationID;
    private String stationName;

    public Station() {
    }

    public Integer getRegionID() {
        return regionID;
    }

    public void setRegionID(Integer regionID) {
        this.regionID = regionID;
    }

    public Integer getSolarSystemID() {
        return solarSystemID;
    }

    public void setSolarSystemID(Integer solarSystemID) {
        this.solarSystemID = solarSystemID;
    }

    public Integer getStationID() {
        return stationID;
    }

    public void setStationID(Integer stationID) {
        this.stationID = stationID;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }
}
