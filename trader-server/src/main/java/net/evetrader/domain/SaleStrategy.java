package net.evetrader.domain;

import java.util.ArrayList;
import java.util.List;

public class SaleStrategy {

    private List<UnitSale> unitSaleList = new ArrayList<>();

    private Double totalSaleAmount;
    private Double totalProfit;
    private Double profitPerJump;

    public SaleStrategy() {
    }

    public List<UnitSale> getUnitSaleList() {
        return unitSaleList;
    }

    public void setUnitSaleList(List<UnitSale> unitSaleList) {
        this.unitSaleList = unitSaleList;
    }

    public Double getTotalSaleAmount() {
        return totalSaleAmount;
    }

    public void setTotalSaleAmount(Double totalSaleAmount) {
        this.totalSaleAmount = totalSaleAmount;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Double getProfitPerJump() {
        return profitPerJump;
    }

    public void setProfitPerJump(Double profitPerJump) {
        this.profitPerJump = profitPerJump;
    }
}
