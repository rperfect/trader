package net.evetrader.domain;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Credentials {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long personId;

    private String passwordHash;

    private String refreshToken;
    private String accessToken;
    private Long accessTokenExpiry;
}
