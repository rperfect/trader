package net.evetrader.domain;


import net.evetrader.services.map.SolarSystem;
import net.troja.eve.esi.model.SystemResponse;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderColumn(name = "unitLoadIndex")
    @JoinColumn(name="shipmentId")
    private List<UnitLoad> unitLoadList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderColumn(name = "unitSaleIndex")
    @JoinColumn(name="shipmentId")
    private List<UnitSale> unitSaleList = new ArrayList<>();

    private Double totalPurchaseCost = 0d;
    private Double totalSaleAmount = 0d;
    private Double totalProfit = 0d;
    private Integer numberOfJumps = 0;
    private Double profitPerJump = 0d;

    private Integer pickupJumpNumber;
    private Float maxDaysToSell = 0f;


    @Transient
    private List<SolarSystem> systemRouteList;

    public Shipment() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<UnitLoad> getUnitLoadList() {
        return unitLoadList;
    }

    public void setUnitLoadList(List<UnitLoad> unitLoadList) {
        this.unitLoadList = unitLoadList;
    }

    public List<UnitSale> getUnitSaleList() {
        return unitSaleList;
    }

    public void setUnitSaleList(List<UnitSale> unitSaleList) {
        this.unitSaleList = unitSaleList;
    }

    public Double getTotalPurchaseCost() {
        return totalPurchaseCost;
    }

    public void setTotalPurchaseCost(Double totalPurchaseCost) {
        this.totalPurchaseCost = totalPurchaseCost;
    }

    public Double getTotalSaleAmount() {
        return totalSaleAmount;
    }

    public void setTotalSaleAmount(Double totalSaleAmount) {
        this.totalSaleAmount = totalSaleAmount;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Integer getNumberOfJumps() {
        return numberOfJumps;
    }

    public void setNumberOfJumps(Integer numberOfJumps) {
        this.numberOfJumps = numberOfJumps;
    }

    public Double getProfitPerJump() {
        return profitPerJump;
    }

    public void setProfitPerJump(Double profitPerJump) {
        this.profitPerJump = profitPerJump;
    }

    public Integer getPickupJumpNumber() {
        return pickupJumpNumber;
    }

    public void setPickupJumpNumber(Integer pickupJumpNumber) {
        this.pickupJumpNumber = pickupJumpNumber;
    }

    public Float getMaxDaysToSell() {
        return maxDaysToSell;
    }

    public void setMaxDaysToSell(Float maxDaysToSell) {
        this.maxDaysToSell = maxDaysToSell;
    }

    public List<SolarSystem> getSystemRouteList() {
        return systemRouteList;
    }

    public void setSystemRouteList(List<SolarSystem> systemRouteList) {
        this.systemRouteList = systemRouteList;
    }
}
