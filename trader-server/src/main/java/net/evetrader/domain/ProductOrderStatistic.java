package net.evetrader.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ProductOrderStatistic {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String timestamp;
    private Integer stationId;
    private String stationName;
    private Integer productId;
    private String productName;

    private Integer marketGroupId;
    private String marketGroupName;
    private String marketParentGroupName;

    private int sellOrderCount;
    private double lowestSellOrderPrice;

    private int buyOrderCount;
    private double highestBuyOrderPrice;

    private double priceDifference;
    private double profitPercentage;
    private int tenDayQuantityTraded;


    public ProductOrderStatistic() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getMarketGroupId() {
        return marketGroupId;
    }

    public void setMarketGroupId(Integer marketGroupId) {
        this.marketGroupId = marketGroupId;
    }

    public String getMarketGroupName() {
        return marketGroupName;
    }

    public void setMarketGroupName(String marketGroupName) {
        this.marketGroupName = marketGroupName;
    }

    public String getMarketParentGroupName() {
        return marketParentGroupName;
    }

    public void setMarketParentGroupName(String marketParentGroupName) {
        this.marketParentGroupName = marketParentGroupName;
    }

    public int getSellOrderCount() {
        return sellOrderCount;
    }

    public void setSellOrderCount(int sellOrderCount) {
        this.sellOrderCount = sellOrderCount;
    }

    public double getLowestSellOrderPrice() {
        return lowestSellOrderPrice;
    }

    public void setLowestSellOrderPrice(double lowestSellOrderPrice) {
        this.lowestSellOrderPrice = lowestSellOrderPrice;
    }

    public int getBuyOrderCount() {
        return buyOrderCount;
    }

    public void setBuyOrderCount(int buyOrderCount) {
        this.buyOrderCount = buyOrderCount;
    }

    public double getHighestBuyOrderPrice() {
        return highestBuyOrderPrice;
    }

    public void setHighestBuyOrderPrice(double highestBuyOrderPrice) {
        this.highestBuyOrderPrice = highestBuyOrderPrice;
    }

    public double getPriceDifference() {
        return priceDifference;
    }

    public void setPriceDifference(double priceDifference) {
        this.priceDifference = priceDifference;
    }

    public double getProfitPercentage() {
        return profitPercentage;
    }

    public void setProfitPercentage(double profitPercentage) {
        this.profitPercentage = profitPercentage;
    }

    public int getTenDayQuantityTraded() {
        return tenDayQuantityTraded;
    }

    public void setTenDayQuantityTraded(int tenDayQuantityTraded) {
        this.tenDayQuantityTraded = tenDayQuantityTraded;
    }
}
