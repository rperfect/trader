package net.evetrader.domain;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RegionToRegionResult {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String scenarioName;

    private Integer fromRegionId;
    private String fromRegionName;
    private Integer toRegionId;
    private String toRegionName;

    private Integer sampleCount;
    private Integer shipmentCount;

    private Double totalPurchaseCost;
    private Double totalSaleAmount;
    private Double totalProfit;

    private Integer totalNumberOfJumps;
    private Double totalProfitPerJump;


    public RegionToRegionResult() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public Integer getFromRegionId() {
        return fromRegionId;
    }

    public void setFromRegionId(Integer fromRegionId) {
        this.fromRegionId = fromRegionId;
    }

    public String getFromRegionName() {
        return fromRegionName;
    }

    public void setFromRegionName(String fromRegionName) {
        this.fromRegionName = fromRegionName;
    }

    public Integer getToRegionId() {
        return toRegionId;
    }

    public void setToRegionId(Integer toRegionId) {
        this.toRegionId = toRegionId;
    }

    public String getToRegionName() {
        return toRegionName;
    }

    public void setToRegionName(String toRegionName) {
        this.toRegionName = toRegionName;
    }

    public Integer getSampleCount() {
        return sampleCount;
    }

    public void setSampleCount(Integer sampleCount) {
        this.sampleCount = sampleCount;
    }

    public Integer getShipmentCount() {
        return shipmentCount;
    }

    public void setShipmentCount(Integer shipmentCount) {
        this.shipmentCount = shipmentCount;
    }

    public Double getTotalPurchaseCost() {
        return totalPurchaseCost;
    }

    public void setTotalPurchaseCost(Double totalPurchaseCost) {
        this.totalPurchaseCost = totalPurchaseCost;
    }

    public Double getTotalSaleAmount() {
        return totalSaleAmount;
    }

    public void setTotalSaleAmount(Double totalSaleAmount) {
        this.totalSaleAmount = totalSaleAmount;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Integer getTotalNumberOfJumps() {
        return totalNumberOfJumps;
    }

    public void setTotalNumberOfJumps(Integer totalNumberOfJumps) {
        this.totalNumberOfJumps = totalNumberOfJumps;
    }

    public Double getTotalProfitPerJump() {
        return totalProfitPerJump;
    }

    public void setTotalProfitPerJump(Double totalProfitPerJump) {
        this.totalProfitPerJump = totalProfitPerJump;
    }
}
