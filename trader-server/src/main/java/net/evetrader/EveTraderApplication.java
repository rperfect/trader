package net.evetrader;

import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import net.evetrader.services.authentication.AuthenticationTokenFilter;
import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.sql.SQLException;

@SpringBootApplication
public class EveTraderApplication {

	@Value("${application.projectId:mycloudrundemo}")
	private String projectId;


	@Bean
	public AuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
		return new AuthenticationTokenFilter();
	}

	public static void startDatabase() {
		try {
			File dataDir = new File("data");
			if(!dataDir.exists()) {
				dataDir.mkdir();
			}
			else if(!dataDir.isDirectory()) {
				throw new RuntimeException("Data directory of " + dataDir.getAbsolutePath() + " exists but is not a directory. Unable to start database.");
			}

			String[] myArgs = {
					"-tcpAllowOthers",
					"-baseDir",
					dataDir.getName()
			};

			Server.createTcpServer(myArgs).start();
		}
		catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Bean
	public Firestore getFirestore() {
		FirestoreOptions firestoreOptions = FirestoreOptions.getDefaultInstance().toBuilder()
						.setProjectId(projectId)
						.build();
		Firestore firestore = firestoreOptions.getService();
		return firestore;
	}

	@Bean
	public Storage getStorage() {
		return StorageOptions.getDefaultInstance().getService();
	}

	public static void main(String[] args) {
		EveTraderApplication.startDatabase();
		SpringApplication.run(EveTraderApplication.class, args);
	}

}
