package net.evetrader.services.job;

public class ProgressMonitor {

    private int completedWorkUnits = 0;
    private int expectedWorkUnits = 10;

    private String jobId;
    private JobRepository jobRepository;

    public ProgressMonitor(String jobId, JobRepository jobRepository) {
        this.jobId = jobId;
        this.jobRepository = jobRepository;
    }

    public synchronized void completeWorkUnits(int numberToComplete) {
        completedWorkUnits += numberToComplete;

        jobRepository.saveJobProgress(jobId, false, getPercentComplete());
    }

    public synchronized int getPercentComplete() {
        int x = completedWorkUnits;
        int y = expectedWorkUnits;
        return Math.round(((float)x / y) * 100);
    }

    public synchronized int getExpectedWorkUnits() {
        return expectedWorkUnits;
    }

    public synchronized void addExpectedWorkUnits(int expectedWorkUnits) {
        this.expectedWorkUnits += expectedWorkUnits;
    }

}
