package net.evetrader.services.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/job")
public class JobController {

    @Autowired
    protected JobService jobService;

    @RequestMapping(value = "/monitor/{jobId}", method = RequestMethod.GET)
    public MonitorResponse monitorJob(@PathVariable String jobId) {
        return jobService.monitorJob(jobId);
    }
}
