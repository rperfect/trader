package net.evetrader.services.job;

public class JobResponse {

    private String jobId;
    private Object result;
    private Exception exception;

    public JobResponse() {
    }

    public JobResponse(String jobId, Object result, Exception exception) {
        this.jobId = jobId;
        this.result = result;
        this.exception = exception;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }
}
