package net.evetrader.services.job;

import java.util.UUID;
import java.util.concurrent.Callable;

public class Job {

    private String jobId;
    private long startTime = System.currentTimeMillis();

    private static ThreadLocal<ProgressMonitor> threadLocal = new ThreadLocal<>();
    private ProgressMonitor progressMonitor;

    private Callable callable;

    private boolean finished = false;

    public Job(String jobId, JobRepository jobRepository, Callable callable) {
        this.jobId = jobId;
        this.callable = callable;
        this.progressMonitor = new ProgressMonitor(jobId, jobRepository);
    }

    public Callable getCallable() {
        return callable;
    }

    public String getJobId() {
        return jobId;
    }

    public long getStartTime() {
        return startTime;
    }

    public static void setProgressMonitor(ProgressMonitor progressMonitor) {
        threadLocal.set(progressMonitor);
    }

    public static ProgressMonitor getProgressMonitor() {
        return threadLocal.get();
    }

    public ProgressMonitor getProgressMonitorFromJob() {
        // needs to be different to the one above so it can be called from a Thread different to the one running the job.
        return progressMonitor;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
