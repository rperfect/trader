package net.evetrader.services.job;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
public class JobRepository {

    @Autowired
    protected Firestore firestore;

    public void saveJobProgress(String jobId, boolean finished, int percentComplete) {
        DocumentReference docRef = firestore.collection("JobProgress").document(jobId);

        Map<String, Object> data = new HashMap<>();
        data.put("jobId", jobId);
        data.put("finished", finished);
        data.put("percentComplete", percentComplete);

        //asynchronously write data
        ApiFuture<WriteResult> result = docRef.set(data);
    }

    public MonitorResponse loadJobProgress(String jobId) {
        try {
            DocumentReference docRef = firestore.collection("JobProgress").document(jobId);

            ApiFuture<DocumentSnapshot> future = docRef.get();
            DocumentSnapshot document = future.get();

            MonitorResponse monitorResponse = null;
            if (document.exists()) {
                Map<String, Object> dataMap = document.getData();

                Integer percentComplete = dataMap.get("percentComplete") != null ? Long.valueOf((long)dataMap.get("percentComplete")).intValue() : null;
                monitorResponse = new MonitorResponse(jobId, (boolean)dataMap.get("finished"), percentComplete);
            }
            else {
                System.out.println("No such document!");
            }

            return monitorResponse;
        }
        catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteJobProgress(String jobId) {
        firestore.collection("JobProgress").document(jobId).delete();
    }
}
