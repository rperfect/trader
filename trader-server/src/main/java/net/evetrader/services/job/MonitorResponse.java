package net.evetrader.services.job;

public class MonitorResponse {

    private String jobId;
    private boolean finished;
    private int percentComplete; // 0-100 whole numbers only


    public MonitorResponse(String jobId, boolean finished, int percentComplete) {
        this.jobId = jobId;
        this.finished = finished;
        this.percentComplete = percentComplete;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public int getPercentComplete() {
        return percentComplete;
    }

    public void setPercentComplete(int percentComplete) {
        this.percentComplete = percentComplete;
    }
}
