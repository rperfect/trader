package net.evetrader.services.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.Callable;

@Component
public class JobService {

    @Autowired
    protected JobRepository jobRepository;

    public JobResponse startJob(String jobId, Callable callable) {
        Job job = new Job(jobId, jobRepository, callable);
        // setup the thread local
        Job.setProgressMonitor(job.getProgressMonitorFromJob());

        Object result = null;
        Exception exception = null;
        try {
            result = job.getCallable().call();
        }
        catch (Exception e) {
            e.printStackTrace();
            exception = e;
        }
        finally {
            Job.setProgressMonitor(null);
            jobRepository.deleteJobProgress(jobId);
        }

        return new JobResponse(job.getJobId(), result, exception);
    }

    public MonitorResponse monitorJob(String jobId) {
        return jobRepository.loadJobProgress(jobId);
    }

}
