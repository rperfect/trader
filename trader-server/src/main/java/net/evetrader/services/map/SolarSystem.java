package net.evetrader.services.map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown=true)
public class SolarSystem {

    private Double security;
    private String securityClass;
    private Integer solarSystemID;
    private String solarSystemName;
    private Star star;
    private Map<Integer, Stargate> stargates;

    public SolarSystem() {
    }

    public Double getSecurity() {
        return security;
    }

    public void setSecurity(Double security) {
        this.security = security;
    }

    public String getSecurityClass() {
        return securityClass;
    }

    public void setSecurityClass(String securityClass) {
        this.securityClass = securityClass;
    }

    public Integer getSolarSystemID() {
        return solarSystemID;
    }

    public void setSolarSystemID(Integer solarSystemID) {
        this.solarSystemID = solarSystemID;
    }

    public String getSolarSystemName() {
        return solarSystemName;
    }

    public void setSolarSystemName(String solarSystemName) {
        this.solarSystemName = solarSystemName;
    }

    public Star getStar() {
        return star;
    }

    public void setStar(Star star) {
        this.star = star;
    }

    public Map<Integer, Stargate> getStargates() {
        return stargates;
    }

    public void setStargates(Map<Integer, Stargate> stargates) {
        this.stargates = stargates;
    }
}
