package net.evetrader.services.map.loader;

import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileObjectReference implements ObjectReference {

    private File file;

    public FileObjectReference(File file) {
        this.file = file;
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public boolean isDirectory() {
        return file.isDirectory();
    }

    @Override
    public byte[] getContent() {
        try {
            return FileCopyUtils.copyToByteArray(file);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ObjectReference> list() {
        String[] nameList = file.list();

        List<ObjectReference> objectReferenceList = new ArrayList<>();
        if(nameList != null && nameList.length > 0) {
            for (String nextName : nameList) {
                File nextFile = new File(file, nextName);
                FileObjectReference f = new FileObjectReference(nextFile);
                objectReferenceList.add(f);
            }
        }

        return objectReferenceList;
    }
}
