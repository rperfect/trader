package net.evetrader.services.map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class Stargate {

    private Integer destination;
    private Long[] position;
    private Integer typeID;

    public Stargate() {
    }

    public Integer getDestination() {
        return destination;
    }

    public void setDestination(Integer destination) {
        this.destination = destination;
    }

    public Long[] getPosition() {
        return position;
    }

    public void setPosition(Long[] position) {
        this.position = position;
    }

    public Integer getTypeID() {
        return typeID;
    }

    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }
}
