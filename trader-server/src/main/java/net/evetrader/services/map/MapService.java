package net.evetrader.services.map;

import net.evetrader.EveTraderApplication;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MapService {

    @Autowired
    protected MapLoader mapLoader;


    private Map<Integer, SolarSystem> solarSystemMap;
    private Map<Integer, SolarSystem> stargateMap;
    private Graph<Integer, DefaultEdge> graphMap;

    public synchronized Map<Integer, SolarSystem> getSolarSystemMap() {
        if (solarSystemMap == null) {
            solarSystemMap = mapLoader.readAllSolarSystems();
        }
        return solarSystemMap;
    }

    public synchronized Map<Integer, SolarSystem> getStargateMap() {
        if (stargateMap == null) {
            stargateMap = new HashMap<>();
            getSolarSystemMap().values().forEach(solarSystem -> {
                solarSystem.getStargates().keySet().forEach(stargateId -> {
                    stargateMap.put(stargateId, solarSystem);
                });
            });
        }
        return stargateMap;
    }

    public synchronized Graph<Integer, DefaultEdge> getGraphMap() {
        if (graphMap == null) {
            graphMap = loadGraphMap();
        }
        return graphMap;
    }

    protected Graph<Integer, DefaultEdge> loadGraphMap() {
        Graph<Integer, DefaultEdge> graph = new DefaultUndirectedGraph<>(DefaultEdge.class);

        // First pass: add the Vertexes
        getSolarSystemMap().keySet().forEach(solarSystemId -> {
            graph.addVertex(solarSystemId);
        });

        // Second pass: add the Edges
        final Map<Integer, SolarSystem> stargateMap = getStargateMap();
        getSolarSystemMap().values().forEach(solarSystem -> {
            solarSystem.getStargates().keySet().forEach(stargateId -> {
                Stargate stargate = solarSystem.getStargates().get(stargateId);
                Integer destinationId = stargate.getDestination();

                if(stargateMap.containsKey(destinationId)) {
                    SolarSystem destinationSystem = stargateMap.get(destinationId);
                    graph.addEdge(solarSystem.getSolarSystemID(), destinationSystem.getSolarSystemID());
                }
            });
        });

        return graph;
    }

    public List<SolarSystem> getShortestPath(Integer fromSystemId, Integer toSystemId) {
        DijkstraShortestPath<Integer, DefaultEdge> dijkstraAlg = new DijkstraShortestPath<>(getGraphMap());
        GraphPath<Integer, DefaultEdge> path = dijkstraAlg.getPath(fromSystemId, toSystemId);

        List<SolarSystem> pathList = new ArrayList<>();
        if(path != null && path.getVertexList() != null) {
            path.getVertexList().forEach(vertex -> {
                SolarSystem solarSystem = getSolarSystemMap().get(vertex);
                pathList.add(solarSystem);
            });
        }

        return pathList;
    }


    public static void main(String[] args) {
        EveTraderApplication.startDatabase();
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);
        MapService mapService = applicationContext.getBean(MapService.class);

        long mapLoadStartTime = System.currentTimeMillis();
        System.out.println("Loading solar systems: Started....");
        List<SolarSystem> solarSystemList = new ArrayList<>(mapService.getSolarSystemMap().values());
        long mapLoadDuration = System.currentTimeMillis() - mapLoadStartTime;
        System.out.println("Loading solar systems: Finished in " + mapLoadDuration + "ms. Solar system count: " + solarSystemList.size());

        for(int i = 0; i < 10; i++) {
            Collections.shuffle(solarSystemList);
            SolarSystem start = solarSystemList.get(0);
            SolarSystem end = solarSystemList.get(1);

            long startTime = System.currentTimeMillis();

            List<SolarSystem> routeList = mapService.getShortestPath(start.getSolarSystemID(), end.getSolarSystemID());
            long duration = System.currentTimeMillis() - startTime;
            if(routeList.size() > 0) {
                System.out.print("Path: " + routeList.size() + ", duration = " + duration +" | ");

                routeList.forEach(solarSystem -> {
                    System.out.print(solarSystem.getSolarSystemName() + " - ");
                });

                System.out.println();
            }
            else {
                System.out.println("Path: " + null);
            }
        }
    }
}
