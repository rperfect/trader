package net.evetrader.services.map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import net.evetrader.services.map.loader.ObjectReference;
import net.evetrader.services.map.loader.StaticDataLoader;
import net.evetrader.services.name.NameCacheService;
import net.evetrader.services.name.NameMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;

@Component
public class MapLoader {
    
    @Value("${appplication.mapSize:small}")
    protected String mapSize;
    
    private String[] SMALL_MAP_FILES = {
            "eve/Domain",
//            "eve/SinqLaison",
//            "eve/TheCitadel",
//            "eve/TheForge",
//            "eve/TheBleakLands",
//            "eve/Heimatar",
//            "eve/Metropolis",
    };
    
    private String[] BIG_MAP_FILES = {
            "eve",
    };

    @Autowired
    @Qualifier("GoogleStaticDataLoader")
    //@Qualifier("FileStaticDataLoader")
    protected StaticDataLoader staticDataLoader;

    @Autowired
    protected NameCacheService nameCacheService;

    private ObjectMapper solarSystemMapper;


    public Map<Integer, SolarSystem> readAllSolarSystems() {
        Map<Integer, SolarSystem> solarSystemMap = new HashMap<>();

        String[] mapDirs = mapSize.equals("small") ? SMALL_MAP_FILES : BIG_MAP_FILES;
        for(String nextDirName : mapDirs) {
            List<ObjectReference> list = staticDataLoader.find(nextDirName);
            readSolarSystems(list, solarSystemMap);
        }

        populateNames(solarSystemMap);

        return solarSystemMap;
    }

    private void readSolarSystems(List<ObjectReference> fromList, Map<Integer, SolarSystem> solarSystemMap) {
        int counter = 0;
        for( ObjectReference file : fromList) {
            if(file.getName().endsWith("solarsystem.staticdata")) {
                SolarSystem solarSystem = loadSolarSystem(file);
                solarSystemMap.put(solarSystem.getSolarSystemID(), solarSystem);
            }
            // else ignore, not a file we are interested in

            if(counter++ % 10 == 0) {
                System.out.print(".");
            }
        }

        System.out.println();
    }

    private SolarSystem loadSolarSystem(ObjectReference file) {
        try {
            TypeReference<SolarSystem> typeRef = new TypeReference<>() {};
            return getSolarSystemMapper().readValue(file.getContent(), typeRef);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private ObjectMapper getSolarSystemMapper() {
        if (solarSystemMapper == null) {
            solarSystemMapper = new ObjectMapper(new YAMLFactory());
        }
        return solarSystemMapper;
    }

    private void populateNames(Map<Integer, SolarSystem> solarSystemMap) {
        Set<Integer> nameSet = new HashSet<>();
        solarSystemMap.values().forEach(solarSystem -> {
            nameSet.add(solarSystem.getSolarSystemID());
        });

        NameMap nameMap = nameCacheService.loadNames(new ArrayList<>(nameSet));
        solarSystemMap.values().forEach(solarSystem -> {
            String name = nameMap.getNameSafely(solarSystem.getSolarSystemID());
            solarSystem.setSolarSystemName(name);
        });
    }

}
