package net.evetrader.services.map.loader;


import com.google.cloud.storage.Storage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("GoogleStaticDataLoader")
public class GoogleStaticDataLoader implements StaticDataLoader {

    @Value("${application.staticData.bucketName:evestaticdata}")
    protected String bucketName;

    @Value("${appplication.staticData.baseDir:sde-20190219-TRANQUILITY/sde/fsd/universe/}")
    protected String baseDir;

    @Autowired
    protected Storage storage;


    @Override
    public List<ObjectReference> find(String namePrefix) {

        String startsWithMatch = baseDir + namePrefix;

        List<ObjectReference> list = new ArrayList<>();
        storage.list(bucketName).iterateAll().forEach(blob -> {
            if(blob.getName().startsWith(startsWithMatch)) {
                list.add(new GoogleObjectReference(blob, storage));
            }
        });

        return list;
    }
}
