package net.evetrader.services.map.loader;

import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Component("FileStaticDataLoader")
public class FileStaticDataLoader implements StaticDataLoader {

    private String baseDir = "C:/Java/sde-20190219-TRANQUILITY/sde/fsd/universe/";

    @Override
    public List<ObjectReference> find(String namePrefix) {
        List<ObjectReference> list = new ArrayList<>();
        File fromDir = new File(baseDir + namePrefix);
        find(list, fromDir);
        return list;
    }

    protected void find(List<ObjectReference> list, File fromDir) {
        File[] files = fromDir.listFiles();
        for(File nextFile : files) {
            if(nextFile.isDirectory()) {
                find(list, nextFile);
            }
            else {
                list.add(new FileObjectReference(nextFile));
            }
        }
    }
}
