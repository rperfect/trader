package net.evetrader.services.map.loader;

import java.util.List;

public interface ObjectReference {

    String getName();
    boolean isDirectory();
    byte[] getContent();

    List<ObjectReference> list();
}
