package net.evetrader.services.map.loader;

import java.util.List;

public interface StaticDataLoader {

    List<ObjectReference> find(String namePrefix);
}
