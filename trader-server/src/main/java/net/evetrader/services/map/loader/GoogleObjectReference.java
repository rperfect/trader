package net.evetrader.services.map.loader;

import com.google.api.gax.paging.Page;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Storage;

import java.util.ArrayList;
import java.util.List;

public class GoogleObjectReference implements ObjectReference {

    private Blob blob;
    private Storage storage;


    public GoogleObjectReference(Blob blob, Storage storage) {
        this.blob = blob;
        this.storage = storage;
    }

    @Override
    public String getName() {
        return blob.getName();
    }

    @Override
    public boolean isDirectory() {
        return blob.isDirectory();
    }

    @Override
    public byte[] getContent() {
        return blob.getContent();
    }

    @Override
    public List<ObjectReference> list() {

        List<ObjectReference> objectList = new ArrayList<>();
        String directoryName = blob.getName();
        Page<Blob> blobs = storage.list("bucketname", Storage.BlobListOption.currentDirectory(), Storage.BlobListOption.prefix(directoryName));
        blobs.iterateAll().forEach(blob -> {
            objectList.add(new GoogleObjectReference(blob, storage));
        });

        return objectList;
    }
}
