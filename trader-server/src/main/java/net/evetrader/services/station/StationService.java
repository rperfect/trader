package net.evetrader.services.station;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import net.evetrader.domain.Station;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.*;

@Component
public class StationService {

    private List<Station> stationList;
    private Map<Integer, Station> stationMap;

    public synchronized List<Station> getStationList() {
        if (stationList == null) {
            stationList = readStationList();
        }
        return stationList;
    }

    private synchronized List<Station> readStationList() {
        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            TypeReference<List<Station>> typeRef = new TypeReference<>() {};

            List<Station> list = mapper.readValue(StationService.class.getResourceAsStream("/staStations.yaml"), typeRef);
            list.sort(Comparator.comparing(Station::getStationName));

            return list;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized Map<Integer, Station> getStationMap() {
        if (stationMap == null) {
            stationMap = new HashMap<>();

            List<Station> list = getStationList();
            list.forEach(station -> {
                stationMap.put(station.getStationID(), station);
            });
        }
        return stationMap;
    }

}
