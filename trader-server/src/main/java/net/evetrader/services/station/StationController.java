package net.evetrader.services.station;

import net.evetrader.domain.Station;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping(value = "/api/stations")
public class StationController {

    @Autowired
    protected StationService stationService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<Station> getStations() {
        return stationService.getStationList();
    }
}
