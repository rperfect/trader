package net.evetrader.services.order;

import net.evetrader.services.api.ApiCallable;
import net.evetrader.services.api.ApiService;
import net.evetrader.services.job.Job;
import net.evetrader.services.job.ProgressMonitor;
import net.evetrader.services.loadboard.LoadBoardService;
import net.evetrader.services.loadboard.OrderMap;
import net.evetrader.services.loadboard.ProductOrderList;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.ApiResponse;
import net.troja.eve.esi.HeaderUtil;
import net.troja.eve.esi.api.MarketApi;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class OrderService {

    @Value("${application.maxOrderPages:10}")
    private int maxOrderPages;


    @Autowired
    protected ApiService apiService;

    @Autowired
    protected OrderMapFactory orderMapFactory;

    public OrderMap getOrderMap(String orderType, Integer regionId, OrderService.SortOrder sortOrder) {
        List<MarketOrdersResponse> currentOrders = getOrderList(orderType, regionId);
        OrderMap orderMap = orderMapFactory.create(currentOrders, sortOrder);
        return orderMap;
    }

    public List<MarketOrdersResponse> getOrderList(String orderType, Integer regionId) {
        try {
            // TODO: need to loop through the pages
            MarketApi marketApi = apiService.getMarketApi();
            List<MarketOrdersResponse> orders = new ArrayList<>();

            ApiResponse<List<MarketOrdersResponse>> response = new ApiCallable<ApiResponse<List<MarketOrdersResponse>>>() {
                @Override
                public ApiResponse<List<MarketOrdersResponse>> callApi() throws ApiException {
                    return marketApi.getMarketsRegionIdOrdersWithHttpInfo(orderType, regionId, null, null, null, null);
                }
            }.call();

            //ApiResponse<List<MarketOrdersResponse>> response = marketApi.getMarketsRegionIdOrdersWithHttpInfo(orderType, regionId, null, null, null, null);
            orders.addAll(response.getData());

            Integer xPages = HeaderUtil.getXPages(response.getHeaders());
            if (xPages == null || xPages < 2) {
                return orders;
            }

            //For each page greater than one. This can be done in threads, but, require a new ApiClient and MarketApi for each thread
            int maxPages = Math.min(maxOrderPages, xPages);
            System.out.println("Downloading " + maxPages + " pages out of " + xPages);
            ProgressMonitor progressMonitor = Job.getProgressMonitor();
            progressMonitor.addExpectedWorkUnits(maxPages);
            progressMonitor.completeWorkUnits(1);

            for (int page = 2; page <= maxPages; page++) {
                //Get market orders

                final int pageNumber = page;
                List<MarketOrdersResponse> pageResponse = new ApiCallable<List<MarketOrdersResponse>>() {
                    @Override
                    public List<MarketOrdersResponse> callApi() throws ApiException {
                        return marketApi.getMarketsRegionIdOrders(orderType, regionId, null, null, pageNumber, null);
                    }
                }.call();


                //List<MarketOrdersResponse> pageResponse = marketApi.getMarketsRegionIdOrders(orderType, regionId, null, null, page, null);
                orders.addAll(pageResponse);

                // sleep for a bit
                //try { Thread.sleep(50); } catch (InterruptedException ex) {}
                progressMonitor.completeWorkUnits(1);
            }

            return orders;
        }
        catch (ApiException e) {
            apiService.logApiException(e);
            throw new RuntimeException(e);
        }
    }

    public enum SortOrder {
        DESCENDING, ASCENDING
    }

}
