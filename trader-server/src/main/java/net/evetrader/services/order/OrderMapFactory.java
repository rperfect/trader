package net.evetrader.services.order;

import net.evetrader.services.loadboard.OrderMap;
import net.evetrader.services.loadboard.ProductOrderList;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

@Component
public class OrderMapFactory {

    public OrderMap create(List<MarketOrdersResponse> orderList, OrderService.SortOrder sortOrder) {

        OrderMap orderMap = new OrderMap();
        for(MarketOrdersResponse nextOrder : orderList) {
            Integer productId = nextOrder.getTypeId();

            ProductOrderList productOrderList = orderMap.getProductOrderMap().get(productId);
            if(productOrderList == null) {
                productOrderList = new ProductOrderList();
                orderMap.getProductOrderMap().put(productId, productOrderList);
            }

            productOrderList.getList().add(nextOrder);
        }

        // sort by price and location
        for(ProductOrderList productOrderList : orderMap.getProductOrderMap().values()) {

            Collections.sort(productOrderList.getList(), (o1, o2) -> {

                Double o1Price = o1.getPrice();
                Double o2Price = o2.getPrice();

                int priceComparison;

                if(sortOrder.equals(OrderService.SortOrder.ASCENDING)) {
                    priceComparison = o1Price.compareTo(o2Price);
                }
                else {
                    priceComparison = o2Price.compareTo(o1Price);
                }

                if(priceComparison != 0) {
                    return priceComparison;
                }
                else {
                    Long o1Location = o1.getLocationId();
                    Long o2Location = o2.getLocationId();

                    int locationComparison = o1Location.compareTo(o2Location);
                    return locationComparison;
                }
            });
        }

        return orderMap;
    }
}
