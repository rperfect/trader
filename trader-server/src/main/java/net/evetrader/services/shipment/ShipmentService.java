package net.evetrader.services.shipment;

import net.evetrader.domain.Shipment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ShipmentService {

    @Autowired
    protected ShipmentRepository shipmentRepository;

    public Shipment save(Shipment shipment) {
        Shipment savedShipment = shipmentRepository.save(shipment);
        return savedShipment;
    }
}
