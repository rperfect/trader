package net.evetrader.services.shipment;

import net.evetrader.domain.Shipment;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ShipmentRepository extends PagingAndSortingRepository<Shipment, Long> {
}
