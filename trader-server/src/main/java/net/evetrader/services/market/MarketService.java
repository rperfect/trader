package net.evetrader.services.market;

import net.evetrader.services.api.ApiCallable;
import net.evetrader.services.api.ApiService;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.model.MarketHistoryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.*;

@Component
public class MarketService {

    @Autowired
    protected ApiService apiService;

    public List<MarketHistoryResponse> getMarketHistory(Integer regionId, Integer productId) {
        try {
            List<MarketHistoryResponse> marketHistory = new ApiCallable<List<MarketHistoryResponse>>() {
                @Override
                public List<MarketHistoryResponse> callApi() throws ApiException {
                    return apiService.getMarketApi().getMarketsRegionIdHistory(regionId, productId, null, null);
                }
            }.call();

            return marketHistory;
        }
        catch (ApiException e) {
            apiService.logApiException(e);
            throw new RuntimeException(e);
        }
    }

    public int getQuantityTraded(Integer regionId, Integer productId, int periodInDays) {
        List<MarketHistoryResponse> marketHistory = getMarketHistory(regionId, productId);
        return getQuantityTraded(marketHistory, periodInDays);
    }

    public int getQuantityTraded(List<MarketHistoryResponse> marketHistory, int periodInDays) {
        int quantityTraded = 0;
        int minIdx = Math.max(marketHistory.size() - 1 - periodInDays, 0);
        for(int i = marketHistory.size() - 1; i > minIdx; i--) {
            quantityTraded += marketHistory.get(i).getVolume();
        }
        return quantityTraded;
    }

    public double getHighestMedianPriceForPeriod(List<MarketHistoryResponse> marketHistory, int periodInDays) {
        List<Double> recentHighestPriceList = new ArrayList<>();
        int minIdx = Math.max(marketHistory.size() - 1 - periodInDays, 0);
        for(int i = marketHistory.size() - 1; i > minIdx; i--) {
            MarketHistoryResponse historyResponse = marketHistory.get(i);
            recentHighestPriceList.add(historyResponse.getHighest());
        }

        recentHighestPriceList.sort(Comparator.naturalOrder());
        Double median = 0d;
        if(recentHighestPriceList.size() > 0) {
            median = recentHighestPriceList.get(recentHighestPriceList.size() / 2);
        }
        return median;
    }

    public double getAverageOrderCount(List<MarketHistoryResponse> marketHistory, int periodInDays) {
        long accumulator = 0;
        int minIdx = Math.max(marketHistory.size() - 1 - periodInDays, 0);
        for(int i = marketHistory.size() - 1; i > minIdx; i--) {
            MarketHistoryResponse historyResponse = marketHistory.get(i);
            accumulator += historyResponse.getOrderCount();
        }

        double averageOrderCount = accumulator / periodInDays;
        return  averageOrderCount;
    }
}
