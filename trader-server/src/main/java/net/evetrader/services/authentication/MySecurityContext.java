package net.evetrader.services.authentication;

public class MySecurityContext {

    private MyUserDetails myUserDetails;

    public MySecurityContext() {
    }

    public MyUserDetails getMyUserDetails() {
        return myUserDetails;
    }

    public void setMyUserDetails(MyUserDetails myUserDetails) {
        this.myUserDetails = myUserDetails;
    }
}
