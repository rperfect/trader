package net.evetrader.services.authentication;

import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

@Component
public class EncryptionService {

    public String encrypt(String clearText, String encryptionKey) {
        try {
            byte[] keyData = encryptionKey.getBytes();
            SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
            byte[] hasil = cipher.doFinal(clearText.getBytes());
            String encryptedText = Base64.getEncoder().encodeToString(hasil);
            return encryptedText;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String decrypt(String encryptedText, String encryptionKey) {
        try {
            byte[] keyData = encryptionKey.getBytes();
            SecretKeySpec secretKeySpec = new SecretKeySpec(keyData, "Blowfish");
            Cipher cipher = Cipher.getInstance("Blowfish");
            cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
            byte[] hasil = cipher.doFinal(Base64.getDecoder().decode(encryptedText));
            String clearText = new String(hasil);
            return clearText;
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {

        String encryptionKey = "SecretKey";
        EncryptionService encryptionService = new EncryptionService();
        String encryptedText = encryptionService.encrypt("Mary had a little lamb.", encryptionKey);
        System.out.println("Encrypted text = " + encryptedText);

        String decryptedText = encryptionService.decrypt(encryptedText, encryptionKey);

        System.out.println("Decrypted text = " + decryptedText);
    }
}
