package net.evetrader.services.authentication;

public class GetAuthorizationUriResponse {

    String uri;

    public GetAuthorizationUriResponse() {
    }

    public GetAuthorizationUriResponse(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
