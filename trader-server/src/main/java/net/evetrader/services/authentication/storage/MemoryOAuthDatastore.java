package net.evetrader.services.authentication.storage;

import net.evetrader.services.authentication.OAuthWorkflow;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class MemoryOAuthDatastore implements OAuthDatastore {

    protected Map<String, OAuthWorkflow> workflowMap = new HashMap<>();


    @Override
    public void saveWorkflow(String state, OAuthWorkflow workflow) {
        workflowMap.put(state, workflow);
    }

    @Override
    public OAuthWorkflow remove(String state) {
        return workflowMap.get(state);
    }
}
