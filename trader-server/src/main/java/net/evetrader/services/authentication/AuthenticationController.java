package net.evetrader.services.authentication;

import net.evetrader.services.api.ApiService;
import net.evetrader.services.authentication.storage.OAuthDatastore;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.auth.JWT;
import net.troja.eve.esi.auth.OAuth;
import net.troja.eve.esi.auth.SsoScopes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/api/oauth")
public class AuthenticationController {

    @Value("${application.oauth.callbackURI:http://localhost:4200/api/oauth/callback}")
    protected String oauthCallbackURI;

    @Autowired
    protected ApiService apiService;

    @Autowired
    protected JwtTokenService jwtTokenService;

    //@Autowired
    //protected OAuthDatastore oAuthDatastore;

    @RequestMapping(value = "/authorizationUri", method = RequestMethod.GET)
    public GetAuthorizationUriResponse getAuthorizationUri() {
        final String state = String.valueOf(System.currentTimeMillis());

        final OAuth auth = (OAuth) apiService.getClient().getAuthentication("evesso");
        final Set<String> scopes = new HashSet<>(Arrays.asList(
                SsoScopes.ESI_MARKETS_READ_CHARACTER_ORDERS_V1
        ));

        //oAuthDatastore.saveWorkflow(state, new OAuthWorkflow(auth, state));

        final String authorizationUri = auth.getAuthorizationUri(oauthCallbackURI, scopes, state);
        return new GetAuthorizationUriResponse(authorizationUri);
    }


    @RequestMapping(value = "/callback", method = RequestMethod.GET)
    public ResponseEntity receiveCallback(HttpServletRequest request) {

        try {
            String code = request.getParameter("code");
            String state = request.getParameter("state");

            //System.out.println("receiveCallback: code = " + code + ", state = " + state);

            //OAuthWorkflow workflow = oAuthDatastore.remove(state);
            //OAuth auth = workflow.getAuth();

            OAuth auth = (OAuth) apiService.getClient().getAuthentication("evesso");
            final Set<String> scopes = new HashSet<>(Arrays.asList(
                    SsoScopes.ESI_MARKETS_READ_CHARACTER_ORDERS_V1
            ));

            final String authorizationUri = auth.getAuthorizationUri(oauthCallbackURI, scopes, state);
            auth.finishFlow(code, state);

            MyUserDetails userDetails = createUserDetailsFromEveLogin(auth);

            String jwtToken = jwtTokenService.generateToken(userDetails);
            String redirectUrl = "/app/callback?jwtToken=" + jwtToken + "&redirect=/someOtherRoute";
            return ResponseEntity.status(HttpStatus.MOVED_PERMANENTLY)
                    .header(HttpHeaders.LOCATION, redirectUrl)
                    .build();
        }
        catch (ApiException e) {
            apiService.logApiException(e);
            throw new RuntimeException(e);
        }
    }

    private MyUserDetails createUserDetailsFromEveLogin(OAuth auth) {

        JWT jwt = auth.getJWT();

        MyUserDetails userDetails = new MyUserDetails();
        userDetails.setCharacterId(jwt.getPayload().getCharacterID());
        userDetails.setCharacterName(jwt.getPayload().getName());
        userDetails.setRefreshToken(auth.getRefreshToken());
        userDetails.setAccessToken(auth.getAccessToken());
        userDetails.setAccessTokenEpiryTime(System.currentTimeMillis() + (15 * 60 * 1000));

        return userDetails;
    }


}
