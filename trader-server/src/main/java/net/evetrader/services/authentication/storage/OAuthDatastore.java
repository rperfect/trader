package net.evetrader.services.authentication.storage;

import net.evetrader.services.authentication.OAuthWorkflow;

public interface OAuthDatastore {

    void saveWorkflow(String state, OAuthWorkflow workflow);

    OAuthWorkflow remove(String state);
}
