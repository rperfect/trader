package net.evetrader.services.authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class JwtTokenService {

    // The only good secret is the one that you have never told anyone. Rather than keeping the secret in a property
    // file this approach will randomly generate a fresh secret each time the server is restarted. This
    // will invalidate any existing tokens and force users to login again, an alternative approach would be to use
    // public/private key signing saveRevision each public key in the database and include the id of the trusted public key
    // in the token so that this service could validate previously generated keys (because it trusts anything in
    // the database).
    @Value("${application.jwt.useRandomSecret:false}")
    private boolean useRandomSecret;
    private String secret;

    @Value("${application.jwt.token.expirationInSecs:43200}") // 12 hours
    private long expirationInSecs;

    @Autowired
    protected EncryptionService encryptionService;

    private static final String CLAIM_KEY_AUTHORITIES = "authorities";
    private static final String CLAIM_NAME = "name";
    private static final String CLAIM_REFRESH_TOKEN = "refreshToken";

    private String getSecret() {
        if (secret == null) {
            if(useRandomSecret) {
                 secret = UUID.randomUUID().toString();
            }
            else {
                secret = "secret";
            }
        }
        return secret;
    }


    public MyUserDetails parseToken(String token) {

        if(token != null && token.startsWith("Bearer ")) {
            token = token.substring("Bearer ".length());
        }

        Jws<Claims> claims = Jwts.parser().setSigningKey(getSecret()).parseClaimsJws(token);
        Date expiryTime = claims.getBody().getExpiration();

        String principalSubject = claims.getBody().getSubject();
        Integer characterId = Integer.parseInt(principalSubject);
        String decryptedRefreshToken = encryptionService.decrypt(claims.getBody().get(CLAIM_REFRESH_TOKEN, String.class), getSecret());

        MyUserDetails userDetails = new MyUserDetails();
        userDetails.setCharacterId(characterId);
        userDetails.setCharacterName(claims.getBody().get(CLAIM_NAME, String.class));
        userDetails.setRefreshToken(decryptedRefreshToken);
        userDetails.setMyExpiryTime(expiryTime);

        return userDetails;
    }


    public String generateToken(MyUserDetails userDetails) {
        String encryptedRefreshToken = encryptionService.encrypt(userDetails.getRefreshToken(), getSecret());
        return Jwts.builder()
                .setSubject(String.valueOf(userDetails.getCharacterId()))
                .setExpiration(new Date(System.currentTimeMillis() + expirationInSecs * 1000))
                .signWith(SignatureAlgorithm.HS512, getSecret())
                .claim(CLAIM_NAME, userDetails.getCharacterName())
                .claim(CLAIM_REFRESH_TOKEN, encryptedRefreshToken)
                .compact();
    }

}