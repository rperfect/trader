package net.evetrader.services.authentication.storage;

import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import net.evetrader.services.authentication.OAuthWorkflow;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

//@Component
public class FirestoreOAuthDatastore implements OAuthDatastore {


    private Firestore db;

    public FirestoreOAuthDatastore() {

    }

    public Firestore getDb() {
        if (db == null) {
        }
        return db;
    }

    @Override
    public void saveWorkflow(String state, OAuthWorkflow workflow) {

        try {
            DocumentReference docRef = db.collection("users").document("alovelace");
            // Add document data  with id "alovelace" using a hashmap
            Map<String, Object> data = new HashMap<>();
            data.put("first", "Ada");
            data.put("last", "Lovelace");
            data.put("born", 1815);

            //asynchronously write data
            ApiFuture<WriteResult> result = docRef.set(data);

            // result.get() blocks on response
            System.out.println("Update time : " + result.get().getUpdateTime());
        }
        catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public OAuthWorkflow remove(String state) {
        try {
            // asynchronously retrieve all users
            ApiFuture<QuerySnapshot> query = db.collection("users").get();

            // query.get() blocks on response
            QuerySnapshot querySnapshot = query.get();
            List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();

            return null;
        }
        catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }
}
