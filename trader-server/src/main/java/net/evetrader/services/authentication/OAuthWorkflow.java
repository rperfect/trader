package net.evetrader.services.authentication;

import net.troja.eve.esi.auth.OAuth;

public class OAuthWorkflow {

    private OAuth auth;
    private String state;


    public OAuthWorkflow(OAuth auth, String state) {
        this.auth = auth;
        this.state = state;
    }

    public OAuth getAuth() {
        return auth;
    }

    public String getState() {
        return state;
    }
}
