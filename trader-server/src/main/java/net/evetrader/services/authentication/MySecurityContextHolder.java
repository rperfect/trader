package net.evetrader.services.authentication;

public class MySecurityContextHolder {

    private static ThreadLocal<MySecurityContext> threadLocal = new ThreadLocal<>();

    public static void set(MySecurityContext mySecurityContext) {
        threadLocal.set(mySecurityContext);
    }

    public static MySecurityContext get() {
        return threadLocal.get();
    }
}
