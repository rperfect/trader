package net.evetrader.services.authentication;

import net.evetrader.services.utils.JsonUtils;
import net.evetrader.services.utils.RestError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class AuthenticationTokenFilter extends OncePerRequestFilter {

    public static final String TOKEN_HEADER = "Authorization";
    public static final String TOKEN_PARAMETER = "jwsToken";

    // This needs to be the REST API call that is used to login, and so needs to be exempted from authentication checks itself.
    public static final String AUTHENTICATION_PATH = "/api/oauth";

    @Autowired
    protected JwtTokenService tokenService;

    @Autowired
    protected JsonUtils jsonUtils;


    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        return request.getServletPath().equals(AUTHENTICATION_PATH);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {

        if(MySecurityContextHolder.get() == null) {
            String authToken = request.getHeader(TOKEN_HEADER) != null ? request.getHeader(TOKEN_HEADER) : request.getParameter(TOKEN_PARAMETER);
            if (authToken != null) {
                try {
                    // If the authToken has been supplied then parse it and deal with any exceptions
                    MyUserDetails userDetails = tokenService.parseToken(authToken);
                    MySecurityContext securityContext = new MySecurityContext();
                    securityContext.setMyUserDetails(userDetails);

                    MySecurityContextHolder.set(securityContext);
                }
                catch (Exception ex) {
                    // TODO: make this catch more specific to the JWT exceptions thrown
                    //writeExceptionResponse(request, response, new JwtAuthenticationException("Invalid token: " + ex.getMessage(), ex));
                    // return early and don't continue with the execution

                    // Hmm, after some usage it seems the best thing to do with an invalid token is to simply ignore it and NOT set
                    // it into the SecurityContextHolder. That way the rest of the system will either get a valid token or not get
                    // any token at all. If the method itself is a public method then having no token is ok and we shouldn't throw
                    // an error at this point in the processing. If the method being called is protected then the security of the
                    // method itself will throw the Unauthorized exception.
                    //return;
                }
            }

            // Authentication check from here
            boolean authenticatedOk = false;
            String uri = request.getRequestURI();
            boolean authenticationRequired = uri.startsWith("/api") && !uri.startsWith(AUTHENTICATION_PATH);
            if(authenticationRequired) {
                MySecurityContext securityContext = MySecurityContextHolder.get();
                if(securityContext != null && securityContext.getMyUserDetails().getMyExpiryTime().after(new Date())) {
                    authenticatedOk = true;
                }
                else {
                    writeExceptionResponse(request, response, new JwtAuthenticationException("Access denied"));
                    MySecurityContextHolder.set(null);
                    return;
                }
            }

            // else there's no authToken or a valid authToken...
            //  ...if there's no authToken, then that's fine because many lower methods don't require authorisation
            //  the methods that do require authorisation will check the token and throw an exception if authorisation is invalid
            try {
                if(!authenticationRequired || authenticatedOk) {
                    chain.doFilter(request, response);
                }
            }
            finally {
                MySecurityContextHolder.set(null);
            }

        }
        else {
            writeExceptionResponse(request, response, new JwtAuthenticationException("Invalid state. SecurityContext has already been set."));
        }
    }

    /**
     * This class can't use the RestResponseEntityExceptionHandler approach since it operates at the Filter level and
     * above the Controller level.
     *
     * @param response
     * @param ex
     * @throws IOException
     */
    private void writeExceptionResponse(HttpServletRequest request, HttpServletResponse response, Exception ex) throws IOException {
        RestError error = new RestError(request.getRequestURI(), HttpStatus.UNAUTHORIZED.value(), ex);
        response.setStatus(error.getStatus());
        response.getWriter().write(jsonUtils.toJson(error));
    }


}
