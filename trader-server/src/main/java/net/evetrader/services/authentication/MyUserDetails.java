package net.evetrader.services.authentication;

import java.util.Date;

public class MyUserDetails {

    private Integer characterId;
    private String characterName;
    private String refreshToken;
    private String accessToken;
    private Long accessTokenEpiryTime;

    private Date myExpiryTime;

    public MyUserDetails() {
    }

    public Integer getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Integer characterId) {
        this.characterId = characterId;
    }

    public String getCharacterName() {
        return characterName;
    }

    public void setCharacterName(String characterName) {
        this.characterName = characterName;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public Long getAccessTokenEpiryTime() {
        return accessTokenEpiryTime;
    }

    public void setAccessTokenEpiryTime(Long accessTokenEpiryTime) {
        this.accessTokenEpiryTime = accessTokenEpiryTime;
    }

    public Date getMyExpiryTime() {
        return myExpiryTime;
    }

    public void setMyExpiryTime(Date myExpiryTime) {
        this.myExpiryTime = myExpiryTime;
    }
}
