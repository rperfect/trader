package net.evetrader.services.name;

import net.evetrader.domain.UniverseName;
import net.troja.eve.esi.model.UniverseNamesResponse;
import org.springframework.stereotype.Component;

@Component
public class UniverseNameMapper {

    public UniverseName toUniverseName(UniverseNamesResponse universeNamesResponse) {
        UniverseName universeName = new UniverseName();
        universeName.setId(universeNamesResponse.getId());
        universeName.setCategory(universeNamesResponse.getCategory().name());
        universeName.setName(universeNamesResponse.getName());
        return universeName;
    }
}
