package net.evetrader.services.name;

import net.evetrader.domain.UniverseName;
import net.evetrader.services.api.ApiCallable;
import net.evetrader.services.api.ApiService;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.model.UniverseNamesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class NameCacheService {

    @Autowired
    protected ApiService apiService;

    @Autowired
    protected UniverseNameMapper universeNameMapper;

    protected NameMap nameMap = new NameMap();


    public NameMap loadNames(List<Integer> idList) {
        List<Integer> filteredList = null;
        try {
            filteredList = new ArrayList<>();
            for(Integer nextId : idList) {
                if(!nameMap.getMap().containsKey(nextId)) {
                    // for some reason we had a case of a negative ID coming through from somewhere
                    if(nextId > 0 && nextId < 108445904) {  //108445904, 60000661
                        filteredList.add(Math.abs(nextId));

                        if(filteredList.size() > 990) {
                            loadNames2(filteredList);
                            filteredList.clear();
                        }
                    }
                }
            }

            if(filteredList.size() > 0) {
                loadNames2(filteredList);
            }

            return nameMap;
        }
        catch (ApiException e) {
            apiService.logApiException(e);
            return nameMap;
        }
    }

    private void loadNames2(List<Integer> idList) throws ApiException {
        List<UniverseNamesResponse> responseList = new ApiCallable<List<UniverseNamesResponse>>() {
            @Override
            public List<UniverseNamesResponse> callApi() throws ApiException {
                return apiService.getUniverseApi().postUniverseNames(idList, null);
            }
        }.call();

        for (UniverseNamesResponse nextResponse : responseList) {
            nameMap.getMap().put(nextResponse.getId(), universeNameMapper.toUniverseName(nextResponse));
        }
    }

    private UniverseName createUnknownName(Integer id) {
        UniverseName universeName = new UniverseName();
        universeName.setName("UNKNOWN NAME");
        universeName.setCategory("UNKNOWN CATEGORY");
        universeName.setId(id);
        return universeName;
    }


}
