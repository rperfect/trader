package net.evetrader.services.name;

import net.evetrader.domain.UniverseName;

import java.util.HashMap;
import java.util.Map;

public class NameMap {

    private Map<Integer, UniverseName> map = new HashMap<>();

    public Map<Integer, UniverseName> getMap() {
        return map;
    }

    public String getNameSafely(Integer id) {
        String name = "UNKNOWN NAME-" + id;
        UniverseName universeName = map.get(id);
        if(universeName != null) {
            name = universeName.getName();
        }
        return name;
    }
}
