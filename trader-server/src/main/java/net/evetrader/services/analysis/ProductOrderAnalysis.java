package net.evetrader.services.analysis;

import net.evetrader.EveTraderApplication;
import net.evetrader.domain.Product;
import net.evetrader.domain.ProductOrderStatistic;
import net.evetrader.domain.Station;
import net.evetrader.services.hub.HubService;
import net.evetrader.services.job.Job;
import net.evetrader.services.loadboard.OrderMap;
import net.evetrader.services.loadboard.ProductOrderList;
import net.evetrader.services.market.MarketService;
import net.evetrader.services.order.OrderService;
import net.evetrader.services.product.MarketGroupService;
import net.evetrader.services.product.ProductService;
import net.troja.eve.esi.model.MarketHistoryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class ProductOrderAnalysis {

    @Autowired
    protected HubService hubService;

    @Autowired
    protected MarketService marketService;

    @Autowired
    protected ProductOrderStatisticRepository productOrderStatisticRepository;

    @Autowired
    protected ProductService productService;

    @Autowired
    protected MarketGroupService marketGroupService;

    public String productOrderAnalysis() {

        System.out.println("ProductOrderAnalysis: starting.");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HHmm");
        String timestamp = dateFormat.format(new Date());
        Job.getProgressMonitor().addExpectedWorkUnits(60000);

        // for each hub
        // find the orders for that hub
        // convert into our productMap
        // for each product create the ProductOrderStatistic

        List<Station> hubStations = hubService.getHubStations();

        for(Station nextHubStation : hubStations) {

            Integer regionId = nextHubStation.getRegionID();

            OrderMap buyOrderMap = hubService.findOrderMapForStation(nextHubStation, "buy", OrderService.SortOrder.DESCENDING);
            OrderMap sellOrderMap = hubService.findOrderMapForStation(nextHubStation, "sell", OrderService.SortOrder.ASCENDING);

            int processedCount = 0;
            int skippedCount = 0;
            int expectedCount = buyOrderMap.getProductOrderMap().keySet().size();

            for(Integer productId : buyOrderMap.getProductOrderMap().keySet()) {

                ProductOrderList buyProductOrders = buyOrderMap.getProductOrderMap().get(productId);
                ProductOrderList sellProductOrders = sellOrderMap.getProductOrderMap().get(productId);

                if(buyProductOrders != null && sellProductOrders != null && buyProductOrders.getList().size() > 0 && sellProductOrders.getList().size() > 0) {

                    List<MarketHistoryResponse> marketHistory = marketService.getMarketHistory(regionId, productId);

                    ProductOrderStatistic p = new ProductOrderStatistic();
                    p.setTimestamp(timestamp);
                    p.setStationId(nextHubStation.getStationID());
                    p.setStationName(nextHubStation.getStationName());

                    p.setProductId(productId);
                    p.setProductName(productService.getProductMap().getProductNameSafely(productId));

                    Product product = productService.getProductMap().getMap().get(productId);
                    if(product != null) {
                        p.setMarketGroupId(product.getMarketGroupID());
                        p.setMarketGroupName(product.getMarketGroupName());
                        p.setMarketParentGroupName(marketGroupService.getMarketGroupMap().getParentGroupName(product.getMarketGroupID()));
                    }

                    p.setBuyOrderCount(buyProductOrders.getList().size());
                    p.setHighestBuyOrderPrice(buyProductOrders.getList().get(0).getPrice());

                    p.setSellOrderCount(sellProductOrders.getList().size());

                    // Changing the logic here to use the lowest of;
                    //   - what's actually been sold over the last few days
                    //   - or the current lowest sell order
                    // Wanting to filter out situations where there's just one last lurking sell order for a high price that nobody will actually pay
                    double highestPriceForPeriod = marketService.getHighestMedianPriceForPeriod(marketHistory, 10);
                    double lowestSellOrderPrice = sellProductOrders.getList().get(0).getPrice();
                    double sellOrderPrice = Math.min(highestPriceForPeriod, lowestSellOrderPrice);
                    p.setLowestSellOrderPrice(sellOrderPrice);


                    double priceDifference = p.getLowestSellOrderPrice() - p.getHighestBuyOrderPrice();
                    double profitPercentage = priceDifference / p.getHighestBuyOrderPrice();

                    p.setPriceDifference(priceDifference);
                    p.setProfitPercentage(profitPercentage);

                    int tenDayQuantityTraded = marketService.getQuantityTraded(marketHistory, 10);
                    p.setTenDayQuantityTraded(tenDayQuantityTraded);

                    double orderAverage = marketService.getAverageOrderCount(marketHistory, 10);

                    boolean tooFewQty = tenDayQuantityTraded < 100;
                    boolean tooFewOrders = orderAverage < 20;
                    boolean tooCostly = p.getHighestBuyOrderPrice() > 1000000;
                    boolean tooMuchTax = priceDifference < (p.getLowestSellOrderPrice() * 0.06);
                    boolean notWorthIt = (tenDayQuantityTraded / 10) * priceDifference < 100000;
                    if(tooFewQty || tooFewOrders || tooCostly || tooMuchTax || notWorthIt  ) {
                        skippedCount++;
                    }
                    else {
                        productOrderStatisticRepository.save(p);
                    }
                }

                if(processedCount++ % 20 == 0) {
                    System.out.println("Hub " + nextHubStation.getStationName() + ": " + processedCount + " + " + skippedCount + "/" + expectedCount);
                }
            }
        }

        return "Finished";
    }

    public static void main(String[] args) {
        EveTraderApplication.startDatabase();
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);

        AnalysisController analysisController = applicationContext.getBean(AnalysisController.class);
        analysisController.productOrderAnalysis();
    }
}
