package net.evetrader.services.analysis;

import net.evetrader.services.job.JobResponse;
import net.evetrader.services.job.JobService;
import net.evetrader.services.job.MonitorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(value = "/api/analysis")
public class AnalysisController {

    @Autowired
    protected JobService jobService;

    @Autowired
    protected AnalysisService analysisService;

    @RequestMapping(value = "/regionToRegion", method = RequestMethod.POST)
    public JobResponse regionToRegionAnalysis() {
        return jobService.startJob(UUID.randomUUID().toString(), () -> AnalysisController.this.analysisService.regionToRegionAnalysis());
    }

    public JobResponse stationOrderCountAnalysis() {
        return jobService.startJob(UUID.randomUUID().toString(), () -> AnalysisController.this.analysisService.stationOrderCountAnalysis());
    }

    public JobResponse productOrderAnalysis() {
        return jobService.startJob(UUID.randomUUID().toString(), () -> AnalysisController.this.analysisService.productOrderAnalysis());
    }
}
