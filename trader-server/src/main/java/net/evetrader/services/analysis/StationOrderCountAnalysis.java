package net.evetrader.services.analysis;

import net.evetrader.EveTraderApplication;
import net.evetrader.domain.StationOrderCountStatistic;
import net.evetrader.domain.UniverseName;
import net.evetrader.services.api.ApiService;
import net.evetrader.services.job.Job;
import net.evetrader.services.name.NameCacheService;
import net.evetrader.services.name.NameMap;
import net.evetrader.services.order.OrderService;
import net.evetrader.services.region.RegionService;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class StationOrderCountAnalysis {

    @Autowired
    protected StationOrderCountRepository repository;

    @Autowired
    protected ApiService apiService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected OrderService orderService;

    @Autowired
    protected NameCacheService nameCacheService;

    public String stationOrderCountAnalysis() {

        System.out.println("StationOrderCountAnalysis: starting.");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HHmm");
        String timestamp = dateFormat.format(new Date());
        Job.getProgressMonitor().addExpectedWorkUnits(AnalysisConstants.EMPIRE_REGIONS.length);

        String[] empireRegions = AnalysisConstants.EMPIRE_REGIONS;
        for(String regionName : empireRegions) {
            Map<Integer, StationOrderCountStatistic> stationMap = readOrders(timestamp, regionName);
            populateTotalAndNames(stationMap);
            saveToDatabase(stationMap);
        }

        System.out.println("StationOrderCountAnalysis: finished.");
        return "Finished";
    }

    private Map<Integer, StationOrderCountStatistic> readOrders(String timestamp, String regionName) {
        Map<Integer, StationOrderCountStatistic> stationMap = new HashMap<>();

        UniverseName region = regionService.getRegionNameMapByName().get(regionName);
        List<MarketOrdersResponse> orderList = orderService.getOrderList("buy", region.getId());
        List<MarketOrdersResponse> sellOrderList = orderService.getOrderList("sell", region.getId());
        orderList.addAll(sellOrderList);

        orderList.forEach(order -> {
            Integer locationId = order.getLocationId().intValue();
            StationOrderCountStatistic statistic = stationMap.get(locationId);
            if(statistic == null) {
                statistic = new StationOrderCountStatistic();
                statistic.setTimestamp(timestamp);
                statistic.setStationId(locationId);
                stationMap.put(locationId, statistic);
            }

            if(order.getIsBuyOrder()) {
                statistic.setBuyCount(statistic.getBuyCount() + 1);
            }
            else {
                statistic.setSellCount(statistic.getSellCount() + 1);
            }
        });

        return stationMap;
    }

    private void populateTotalAndNames(Map<Integer, StationOrderCountStatistic> stationMap) {

        Set<Integer> idSet = new HashSet<>();
        stationMap.values().forEach(statistic -> {
            idSet.add(statistic.getStationId());
        });

        NameMap nameMap = nameCacheService.loadNames(new ArrayList(idSet));
        stationMap.values().forEach(statistic -> {
            statistic.setStationName(nameMap.getNameSafely(statistic.getStationId()));
            statistic.setTotalCount(statistic.getBuyCount() + statistic.getSellCount());
        });

    }

    private void saveToDatabase(Map<Integer, StationOrderCountStatistic> stationMap) {
        stationMap.values().forEach(statistic -> {
            repository.save(statistic);
        });
    }

    public static void main(String[] args) throws Exception {
        EveTraderApplication.startDatabase();
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);
        AnalysisController analysisController = applicationContext.getBean(AnalysisController.class);
        analysisController.stationOrderCountAnalysis();
    }
}
