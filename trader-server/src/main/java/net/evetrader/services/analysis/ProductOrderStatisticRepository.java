package net.evetrader.services.analysis;

import net.evetrader.domain.ProductOrderStatistic;
import org.springframework.data.repository.CrudRepository;

public interface ProductOrderStatisticRepository extends CrudRepository<ProductOrderStatistic, Integer> {
}
