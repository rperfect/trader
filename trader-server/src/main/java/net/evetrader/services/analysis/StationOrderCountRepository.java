package net.evetrader.services.analysis;

import net.evetrader.domain.StationOrderCountStatistic;
import org.springframework.data.repository.CrudRepository;

public interface StationOrderCountRepository extends CrudRepository<StationOrderCountStatistic, Integer> {
}
