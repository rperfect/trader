package net.evetrader.services.analysis;

public class AnalysisConstants {

    public static final String[] EMPIRE_REGIONS = {
            "Aridia",
            "Devoid",
            "Domain",
            "Genesis",
            "Kador",
            "Kor-Azor",
            "Tash-Murkon",
            "The Bleak Lands",
            "Lonetrek",
            "The Citadel",
            "The Forge",
            "Essence",
            "Everyshore",
            "Placid",
            "Sinq Laison",
            "Solitude",
            "Verge Vendor",
            "Metropolis",
            "Heimatar",
            "Molden Heath",
    };

}
