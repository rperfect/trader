package net.evetrader.services.analysis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class AnalysisService {


    @Autowired
    protected RegionToRegionAnalysis regionToRegionAnalysis;

    @Autowired
    protected StationOrderCountAnalysis stationOrderCountAnalysis;

    @Autowired
    protected ProductOrderAnalysis productOrderAnalysis;

    public String regionToRegionAnalysis() {
        return regionToRegionAnalysis.regionToRegionAnalysis();
    }

    public String stationOrderCountAnalysis() {
        return stationOrderCountAnalysis.stationOrderCountAnalysis();
    }


    public String productOrderAnalysis() {
        return productOrderAnalysis.productOrderAnalysis();
    }
}
