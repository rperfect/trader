package net.evetrader.services.analysis;

import net.evetrader.EveTraderApplication;
import net.evetrader.domain.RegionToRegionResult;
import net.evetrader.domain.Shipment;
import net.evetrader.domain.UniverseName;
import net.evetrader.services.job.Job;
import net.evetrader.services.loadboard.LoadBoardService;
import net.evetrader.services.loadboard.LoadSearchRequest;
import net.evetrader.services.loadboard.ShipmentSearchResponse;
import net.evetrader.services.region.RegionService;
import net.evetrader.services.shipment.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class RegionToRegionAnalysis {

    @Autowired
    protected ShipmentService shipmentService;

    @Autowired
    protected LoadBoardService loadBoardService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected RegionToRegionResultRepository regionToRegionResultRepository;

    public String regionToRegionAnalysis() {

        String[] empireRegions = AnalysisConstants.EMPIRE_REGIONS;
        Job.getProgressMonitor().addExpectedWorkUnits(empireRegions.length * empireRegions.length);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd.HHmm");
        String scenarioName = "RegionToRegion: " + dateFormat.format(new Date());
        int sampleCount = 10;

        Map<String, UniverseName> regionNameMapByName = regionService.getRegionNameMapByName();
        for(String fromRegionName : empireRegions) {
            UniverseName fromRegion = regionNameMapByName.get(fromRegionName);

            for(String toRegionName : empireRegions) {
                UniverseName toRegion = regionNameMapByName.get(toRegionName);

                LoadSearchRequest loadSearchRequest = new LoadSearchRequest();
                loadSearchRequest.setFromRegion(fromRegion);
                loadSearchRequest.setToRegion(toRegion);
                loadSearchRequest.setShipCapacity(13260);
                loadSearchRequest.setIskLimit(20000000d);

                System.out.println(new Date() + ": AnalysisService: from " + fromRegionName + " to " + toRegionName);

                // TODO: Do we want to save the whole ShipmentSearchResponse (for parameters used...)
                ShipmentSearchResponse shipmentSearchResponse = loadBoardService.search(loadSearchRequest);
                List<Shipment> shipmentList = shipmentSearchResponse.getShipmentList();

                for(int i = 0; i < sampleCount && i < shipmentList.size(); i++) {
                    shipmentService.save(shipmentList.get(i));
                }

                RegionToRegionResult regionToRegionResult = createRegionToRegionResult(
                        scenarioName,
                        fromRegion.getId(),
                        fromRegionName,
                        toRegion.getId(),
                        toRegionName,
                        sampleCount,
                        shipmentList);
                regionToRegionResultRepository.save(regionToRegionResult);

                Job.getProgressMonitor().completeWorkUnits(1);
            }
        }

        return "Finished: " + dateFormat.format(new Date());
    }

    private RegionToRegionResult createRegionToRegionResult(String scenarioName,
                                                            Integer fromRegionId,
                                                            String fromRegionName,
                                                            Integer toRegionId,
                                                            String toRegionName,
                                                            Integer sampleCount,
                                                            List<Shipment> shipmentList) {

        RegionToRegionResult result = new RegionToRegionResult();

        result.setScenarioName(scenarioName);
        result.setFromRegionId(fromRegionId);
        result.setFromRegionName(fromRegionName);
        result.setToRegionId(toRegionId);
        result.setToRegionName(toRegionName);

        result.setSampleCount(sampleCount);
        result.setShipmentCount(shipmentList.size());

        Double totalPurchaseCost = 0d;
        Double totalSaleAmount = 0d;
        Double totalProfit = 0d;

        Integer totalNumberOfJumps = 0;
        Double totalProfitPerJump = 0d;

        for(int i = 0; i < sampleCount && i < shipmentList.size(); i++) {
            Shipment shipment = shipmentList.get(i);
            totalPurchaseCost += shipment.getTotalPurchaseCost();
            totalSaleAmount += shipment.getTotalSaleAmount();
            totalProfit += shipment.getTotalProfit();
            totalNumberOfJumps = shipment.getNumberOfJumps();
            totalProfitPerJump = shipment.getProfitPerJump();
        }

        result.setTotalPurchaseCost(totalPurchaseCost);
        result.setTotalSaleAmount(totalSaleAmount);
        result.setTotalProfit(totalProfit);
        result.setTotalNumberOfJumps(totalNumberOfJumps);
        result.setTotalProfitPerJump(totalProfitPerJump);

        return result;
    }

    public static void main(String[] args) throws Exception {
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);
        AnalysisService analysisService = applicationContext.getBean(AnalysisService.class);
        analysisService.regionToRegionAnalysis();
        System.exit(0);
    }


}
