package net.evetrader.services.analysis;

import net.evetrader.domain.RegionToRegionResult;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RegionToRegionResultRepository extends PagingAndSortingRepository<RegionToRegionResult, Integer> {

}
