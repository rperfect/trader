package net.evetrader.services.person;

import net.evetrader.EveTraderApplication;
import net.evetrader.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class PersonService {


    @Autowired
    protected PersonRepository personRepository;

    public void testPerson() {
        System.out.println("Using: " + System.getProperty("spring.datasource.url"));

        Person person = new Person();
        person.setName("John Smith " + System.currentTimeMillis());
        Person personAfterSave = personRepository.save(person);

        personRepository.findById(personAfterSave.getId()).ifPresent( (personFound) -> {
            System.out.println("Found Person: " + personFound.getId() + " - " + personFound.getName());
        });
    }

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);
        PersonService personService = applicationContext.getBean(PersonService.class);
        personService.testPerson();
    }
}
