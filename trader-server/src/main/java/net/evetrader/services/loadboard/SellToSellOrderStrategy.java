package net.evetrader.services.loadboard;

import net.evetrader.domain.SaleStrategy;
import net.evetrader.domain.Shipment;
import net.evetrader.domain.UnitLoad;
import net.evetrader.domain.UnitSale;
import net.evetrader.services.api.ApiService;
import net.evetrader.services.order.OrderService;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.api.MarketApi;
import net.troja.eve.esi.model.MarketHistoryResponse;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SellToSellOrderStrategy implements OrderStrategy {

    @Autowired
    protected ApiService apiService;


    @Autowired
    protected OrderService orderService;

    public OrderMap findSourceOrderMap(LoadSearchRequest loadSearchRequest) {
        return orderService.getOrderMap("sell", loadSearchRequest.getFromRegion().getId(), OrderService.SortOrder.ASCENDING);
    }

    public OrderMap findTargetOrderMap(LoadSearchRequest loadSearchRequest) {
        // For the target orders we want the Sell orders from the "to" region sorted with cheapest first, because we are going to
        // undercut existing orders by some amount
        return orderService.getOrderMap("sell", loadSearchRequest.getToRegion().getId(), OrderService.SortOrder.ASCENDING);
    }

    @Override
    public int getPurchaseOrderQuantity(int sellOrderQuantity, int buyOrderQuantity) {
        // for sell orders we take everything we can get and wait for it to sell
        return sellOrderQuantity;
    }

    @Override
    public double getTargetOrderPrice(MarketOrdersResponse order) {
        return order.getPrice() - 1.0d;
    }

    public double getSellOrderQuantity() {
        return 0d;
    }





    /**
     * Instead of selling to a Buy order, what if we placed our own Sell order in the market at something less than
     * the current lowest sell order. The lowest sell order has to be better than the highest buy order so we should
     * be able to make more profit. The trade-off is that there's more risk and it will take longer to sell. Lets
     * provide an estimate of the time to sell based on recent sales quantity.
     *
     * @param shipment
     */
    public void sellToSellOrder(Shipment shipment, Integer regionId, OrderMap sellOrderMap) {

        // TODO: only works if we have single type of product in a shipment
        Integer productId = shipment.getUnitLoadList().get(0).getProductId();
        int totalUnitLoadQuantity = 0; // the total quantity available in this shipment for this product
        for(UnitLoad unitLoad : shipment.getUnitLoadList()) {
            totalUnitLoadQuantity += unitLoad.getQuantity();
        }

        double accumulatedSalesTotal = 0;

        ProductOrderList productOrderList = sellOrderMap.getProductOrderMap().get(productId);
        /*if(productOrderList != null) {
            List<MarketOrdersResponse> marketOrders = productOrderList.getList();

            SaleStrategy saleStrategy = shipment.getSellOrderSaleStrategy();
            for(MarketOrdersResponse nextOrder : marketOrders) {
                Long locationId = nextOrder.getLocationId();
                Double salePrice = nextOrder.getPrice();  // this is a sell order price
                int volumeRemain = nextOrder.getVolumeRemain();

                UnitSale unitSale = new UnitSale();
                unitSale.setProductId(productId);
                unitSale.setToLocationId(locationId.intValue());
                unitSale.setSaleQuantity(totalUnitLoadQuantity);  // we can sell everything to one location (it just might take longer)
                unitSale.setAvailableQuantity(volumeRemain);
                unitSale.setSalePrice(salePrice);

                // calculate the time to sell this quantity at this location
                //float daysToSell = getDailyAverageQuantity(totalUnitLoadQuantity, regionId, productId);

                saleStrategy.getUnitSaleList().add(unitSale);

                accumulatedSalesTotal = accumulatedSalesTotal + unitSale.getSaleQuantity() * salePrice;

                // TODO: break anyway
                break;
            }

            saleStrategy.setTotalSaleAmount(accumulatedSalesTotal);
            saleStrategy.setTotalProfit(accumulatedSalesTotal - shipment.getTotalPurchaseCost());
        }*/
    }

    @Override
    public boolean daysToSellAvailable() {
        return false;
    }

    @Override
    public float getDailyAverageQuantity(Integer regionId, Integer productId) {
        return 0;
    }
}
