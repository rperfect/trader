package net.evetrader.services.loadboard;

import net.troja.eve.esi.model.MarketOrdersResponse;

import java.util.ArrayList;
import java.util.List;

public class ProductOrderList {

    private List<MarketOrdersResponse> list = new ArrayList<>();

    public ProductOrderList() {
    }

    public List<MarketOrdersResponse> getList() {
        return list;
    }

    public void setList(List<MarketOrdersResponse> list) {
        this.list = list;
    }
}
