package net.evetrader.services.loadboard;

import com.fasterxml.jackson.annotation.JsonInclude;
import net.evetrader.domain.UniverseName;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoadSearchRequest {

    private String mode; // Station, Region
    private String strategy; // SellToBuyOrder, SellToSellOrder

    private Integer fromLocationId;

    private UniverseName fromRegion;
    private Integer shipCapacity;
    private Double iskLimit;
    private String productStrategy;
    private String stationStrategy;

    private Integer toLocationId;
    private UniverseName toRegion;
    private String stationDeliveryStrategy;

    private boolean daysToSellAvailable;

    public LoadSearchRequest() {
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getStrategy() {
        return strategy;
    }

    public void setStrategy(String strategy) {
        this.strategy = strategy;
    }

    public Integer getFromLocationId() {
        return fromLocationId;
    }

    public void setFromLocationId(Integer fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    public UniverseName getFromRegion() {
        return fromRegion;
    }

    public void setFromRegion(UniverseName fromRegion) {
        this.fromRegion = fromRegion;
    }

    public Integer getShipCapacity() {
        return shipCapacity;
    }

    public void setShipCapacity(Integer shipCapacity) {
        this.shipCapacity = shipCapacity;
    }

    public Double getIskLimit() {
        return iskLimit;
    }

    public void setIskLimit(Double iskLimit) {
        this.iskLimit = iskLimit;
    }

    public String getProductStrategy() {
        return productStrategy;
    }

    public void setProductStrategy(String productStrategy) {
        this.productStrategy = productStrategy;
    }

    public String getStationStrategy() {
        return stationStrategy;
    }

    public void setStationStrategy(String stationStrategy) {
        this.stationStrategy = stationStrategy;
    }

    public Integer getToLocationId() {
        return toLocationId;
    }

    public void setToLocationId(Integer toLocationId) {
        this.toLocationId = toLocationId;
    }

    public UniverseName getToRegion() {
        return toRegion;
    }

    public void setToRegion(UniverseName toRegion) {
        this.toRegion = toRegion;
    }

    public String getStationDeliveryStrategy() {
        return stationDeliveryStrategy;
    }

    public void setStationDeliveryStrategy(String stationDeliveryStrategy) {
        this.stationDeliveryStrategy = stationDeliveryStrategy;
    }

    public boolean isDaysToSellAvailable() {
        return daysToSellAvailable;
    }

    public void setDaysToSellAvailable(boolean daysToSellAvailable) {
        this.daysToSellAvailable = daysToSellAvailable;
    }
}
