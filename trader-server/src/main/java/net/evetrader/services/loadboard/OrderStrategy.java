package net.evetrader.services.loadboard;


import net.troja.eve.esi.model.MarketOrdersResponse;

public interface OrderStrategy {

    OrderMap findSourceOrderMap(LoadSearchRequest loadSearchRequest);

    OrderMap findTargetOrderMap(LoadSearchRequest loadSearchRequest);

    double getTargetOrderPrice(MarketOrdersResponse order);

    int getPurchaseOrderQuantity(int sellOrderQuantity, int buyOrderQuantity);

    boolean daysToSellAvailable();

    float getDailyAverageQuantity(Integer regionId, Integer productId);
}
