package net.evetrader.services.loadboard;

import net.evetrader.services.api.ApiCallable;
import net.evetrader.services.api.ApiService;
import net.evetrader.services.map.MapService;
import net.evetrader.services.map.SolarSystem;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.model.StationResponse;
import net.troja.eve.esi.model.SystemResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class RouteFinderService {

    @Value("${application.maxRouteLength:5}")
    protected int maxRouteLength;

    @Autowired
    protected ApiService apiService;

    @Autowired
    protected MapService mapService;

    public List<SolarSystem> findRoute(Integer startingSystemId, List<Integer> fromLocationList, List<Integer> toLocationList) {
        try {
            List<SolarSystem> systemRouteList = new ArrayList<>();

            List<Integer> fromSystemList = toSystemList(fromLocationList);
            List<Integer> toSystemList = toSystemList(toLocationList);

            if(fromSystemList.size() == 0 || toSystemList.size() == 0) {
                return systemRouteList;
            }

            if(fromSystemList.size() > 1) {
                for(int i = 0; i < fromSystemList.size() - 1; i++) {
                    Integer a = fromSystemList.get(i);
                    Integer b = fromSystemList.get(i+1);
                    List<SolarSystem> routeList = findRoute(a, b);
                    addRoutes(systemRouteList, routeList);
                }
            }

            {
                Integer a = fromSystemList.get(fromSystemList.size() - 1);
                Integer b = toSystemList.get(0);
                List<SolarSystem> routeList = findRoute(a, b);
                addRoutes(systemRouteList, routeList);
            }

            if(toSystemList.size() > 1) {
                for(int i = 0; i < toSystemList.size() - 1; i++) {
                    Integer a = toSystemList.get(i);
                    Integer b = toSystemList.get(i+1);
                    List<SolarSystem> routeList = findRoute(a, b);
                    addRoutes(systemRouteList, routeList);
                }
            }

            return systemRouteList;
        }
        catch (ApiException e) {
            apiService.logApiException(e);
            throw new RuntimeException(e);
        }
    }

    private void addRoutes(List<SolarSystem> listOne, List<SolarSystem> listTwo) {
        SolarSystem listOneLastSystem = listOne.size() > 0 ? listOne.get(listOne.size() - 1) : null;
        SolarSystem listTwoFirstSystem = listTwo.size() > 0 ? listTwo.get(0) : null;

        boolean notNull = listOneLastSystem != null && listTwoFirstSystem != null;
        if( notNull && listOneLastSystem.getSolarSystemID().equals(listTwoFirstSystem.getSolarSystemID())) {
            if(listTwo.size() > 1) {
                listOne.addAll(listTwo.subList(1, listTwo.size() - 1));
            }
            // else listTwo system is the same as the last list one system, so we just don't need to add anything
        }
        else {
            // either null, or not matching so just add the whole list
            listOne.addAll(listTwo);
        }
    }

    private List<Integer> toSystemList(List<Integer> locationList) {
        List<Integer> systemList = new ArrayList<>();

        int maxCount = Math.min(locationList.size(), maxRouteLength);
        for(int i = 0; i < maxCount; i++) {
            Integer locationId = locationList.get(i);
            if(LoadBoardService.between(locationId, 0, 108445904)) {
                systemList.add(getSystemIdForLocation(locationId));
            }
        }

        return systemList;
    }

    private Integer getSystemIdForLocation(Integer locationId) {
        try {
            StationResponse stationResponse = new ApiCallable<StationResponse>() {
                @Override
                public StationResponse callApi() throws ApiException {
                    return apiService.getUniverseApi().getUniverseStationsStationId(locationId, null, null);
                }
            }.call();
            return stationResponse.getSystemId();
        }
        catch (ApiException e) {
            apiService.logApiException(e);
            throw new RuntimeException(e);
        }
    }

    private List<SolarSystem> findRoute(Integer fromSystemId, Integer toSystemId) throws ApiException {
        List<SolarSystem> routeList = mapService.getShortestPath(fromSystemId, toSystemId);
        return routeList;
    }
}
