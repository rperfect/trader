package net.evetrader.services.loadboard;

import net.evetrader.domain.Shipment;
import net.evetrader.services.map.SolarSystem;
import net.evetrader.services.order.OrderMapFactory;
import net.evetrader.services.order.OrderService;
import net.evetrader.services.region.RegionService;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class StationToStationStrategy  {

    @Autowired
    protected RouteFinderService routeFinderService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected OrderService orderService;

    @Autowired
    protected OrderMapFactory orderMapFactory;

    @Autowired
    protected LoadBoardService loadBoardService;


    public ShipmentSearchResponse search(LoadSearchRequest loadSearchRequest) {
        // from one station to another, find the shortest (safest?) route

        List<Integer> fromLocationList = Collections.singletonList(loadSearchRequest.getFromLocationId());
        List<Integer> toLocationList = Collections.singletonList(loadSearchRequest.getToLocationId());

        List<SolarSystem> route = routeFinderService.findRoute(null, fromLocationList, toLocationList);

        Set<Integer> systemSet = new HashSet<>();
        Set<Integer> regionSet = new HashSet<>();
        route.forEach(solarSystem -> {
            systemSet.add(solarSystem.getSolarSystemID());
            Integer regionId = regionService.findRegionIdFromSystemId(solarSystem.getSolarSystemID());
            regionSet.add(regionId);
        });

        List<MarketOrdersResponse> regionalSourceOrderList = new ArrayList<>();
        List<MarketOrdersResponse> regionalTargetOrderList = new ArrayList<>();
        regionSet.forEach(regionId -> {
            regionalSourceOrderList.addAll(orderService.getOrderList("sell", regionId));
            regionalTargetOrderList.addAll(orderService.getOrderList("buy", regionId));
        });


        // These System order maps are keyed by SystemId and not ProductId
        List<MarketOrdersResponse> systemSourceOrderList = filterBySystemId(systemSet, regionalSourceOrderList);
        List<MarketOrdersResponse> systemTargetOrderList = filterBySystemId(systemSet, regionalTargetOrderList);


        // For each step of the route attempt to find profitable shipments later in the route
        List<Shipment> shipmentList = new ArrayList<>();
        for(int i = 0; i < route.size() - 1; i++) {  // we won't attempt to buy anything in the last system (but do need to sell into it)
            SolarSystem solarSystem = route.get(i);

            // grab all of the sell orders in this system
            Set<Integer> currentSystem = new HashSet<>(Collections.singletonList(solarSystem.getSolarSystemID()));
            List<MarketOrdersResponse> sourceOrders = filterBySystemId(currentSystem, systemSourceOrderList);

            // grab all of the buy orders along the rest of the current route
            Set<Integer> targetSystems = findSystemsFromRouteIdx(route, i);
            List<MarketOrdersResponse> targetOrders = filterBySystemId(targetSystems, systemTargetOrderList);

            OrderMap sourceOrderMap = orderMapFactory.create(sourceOrders, OrderService.SortOrder.ASCENDING);
            OrderMap targetOrderMap = orderMapFactory.create(targetOrders, OrderService.SortOrder.DESCENDING);

            OrderStrategy orderStrategy = new SellToBuyOrderStrategy();
            List<Shipment> shipments = loadBoardService.findShipments(orderStrategy, loadSearchRequest, sourceOrderMap, targetOrderMap);

            loadBoardService.populateNames(shipments);

            shipments.sort((o1, o2) -> o2.getTotalProfit().compareTo(o1.getTotalProfit()));

            final Integer pickupJumpNumber = i;
            shipments.forEach(shipment -> {
                shipment.setPickupJumpNumber(pickupJumpNumber);
            });

            shipmentList.addAll(shipments);
        }

        return new ShipmentSearchResponse(loadSearchRequest, shipmentList);
    }

    private Set<Integer> findSystemsFromRouteIdx(List<SolarSystem> routeList, int routeIdx) {
        Set<Integer> systemSet = new HashSet<>();
        for(int i = routeIdx; i < routeList.size(); i++) {
            systemSet.add(routeList.get(i).getSolarSystemID());
        }
        return systemSet;
    }

    private List<MarketOrdersResponse> filterBySystemId(Set<Integer> systemSet, List<MarketOrdersResponse> orderList) {
        List<MarketOrdersResponse> filteredList = new ArrayList<>();
        orderList.forEach(marketOrdersResponse -> {
            if(systemSet.contains(marketOrdersResponse.getSystemId())) {
                filteredList.add(marketOrdersResponse);
            }
        });

        return filteredList;
    }




}
