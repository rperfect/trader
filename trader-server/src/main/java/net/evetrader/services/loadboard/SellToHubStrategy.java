package net.evetrader.services.loadboard;

import net.evetrader.services.api.ApiService;
import net.evetrader.services.hub.HubService;
import net.evetrader.services.market.MarketService;
import net.evetrader.services.order.OrderService;
import net.troja.eve.esi.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SellToHubStrategy implements OrderStrategy {

    @Autowired
    protected OrderService orderService;

    @Autowired
    protected HubService hubService;

    @Autowired
    protected MarketService marketService;

    @Override
    public OrderMap findSourceOrderMap(LoadSearchRequest loadSearchRequest) {
        return orderService.getOrderMap("sell", loadSearchRequest.getFromRegion().getId(), OrderService.SortOrder.ASCENDING);
    }

    @Override
    public OrderMap findTargetOrderMap(LoadSearchRequest loadSearchRequest) {
        return hubService.findOrderMapForHubs("sell", OrderService.SortOrder.ASCENDING);
    }

    @Override
    public int getPurchaseOrderQuantity(int sellOrderQuantity, int buyOrderQuantity) {
        // for sell to hub we take everything we can get and wait for it to sell at the hub
        return sellOrderQuantity;
    }

    @Override
    public double getTargetOrderPrice(MarketOrdersResponse order) {
        return order.getPrice() - 1.0d;
    }

    @Override
    public boolean daysToSellAvailable() {
        return true;
    }

    public float getDailyAverageQuantity(Integer regionId, Integer productId) {

        int numberOfDays = 10;
        int quantityTraded = marketService.getQuantityTraded(regionId, productId, numberOfDays);
        float dailyAverage = ((float)quantityTraded) / numberOfDays;

        return dailyAverage;
    }

}
