package net.evetrader.services.loadboard;

import net.evetrader.services.order.OrderService;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SellToBuyOrderStrategy implements OrderStrategy {

    @Autowired
    protected OrderService orderService;

    @Override
    public OrderMap findSourceOrderMap(LoadSearchRequest loadSearchRequest) {
        return orderService.getOrderMap("sell", loadSearchRequest.getFromRegion().getId(), OrderService.SortOrder.ASCENDING);
    }

    @Override
    public OrderMap findTargetOrderMap(LoadSearchRequest loadSearchRequest) {
        // In a sell to buy order we want the buy orders from target region and we want the biggest first
        return orderService.getOrderMap("buy", loadSearchRequest.getToRegion().getId(), OrderService.SortOrder.DESCENDING);
    }

    @Override
    public int getPurchaseOrderQuantity(int sellOrderQuantity, int buyOrderQuantity) {
        return Math.min(sellOrderQuantity, buyOrderQuantity);
    }

    @Override
    public double getTargetOrderPrice(MarketOrdersResponse order) {
        return order.getPrice();
    }

    @Override
    public boolean daysToSellAvailable() {
        return false;
    }

    @Override
    public float getDailyAverageQuantity(Integer regionId, Integer productId) {
        return 0;
    }
}
