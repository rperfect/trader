package net.evetrader.services.loadboard;

import net.evetrader.services.job.JobResponse;
import net.evetrader.services.job.JobService;
import net.evetrader.services.job.MonitorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/load-board")
public class LoadBoardController {

    @Autowired
    protected JobService jobService;

    @Autowired
    protected LoadBoardService loadBoardService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public JobResponse search(@RequestBody LoadSearchRequest loadSearchRequest, @RequestParam String jobId) {
        return jobService.startJob(jobId, () -> LoadBoardController.this.loadBoardService.search(loadSearchRequest));
    }

}
