package net.evetrader.services.loadboard;

import net.evetrader.domain.*;
import net.evetrader.services.job.Job;
import net.evetrader.services.map.SolarSystem;
import net.evetrader.services.name.NameCacheService;
import net.evetrader.services.name.NameMap;
import net.evetrader.services.product.ProductMap;
import net.evetrader.services.product.ProductService;
import net.evetrader.services.region.RegionService;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class LoadBoardService implements ApplicationContextAware {

    @Value("${application.findRouteCount:20}")
    protected int findRouteCount;

    @Autowired
    protected NameCacheService nameCacheService;

    @Autowired
    protected ProductService productService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected RouteFinderService routeFinderService;

    @Autowired
    protected StationToStationStrategy stationToStationStrategy;

    protected ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public ShipmentSearchResponse search(LoadSearchRequest loadSearchRequest) {
        System.out.println("Search: mode = " + loadSearchRequest.getMode());

        String mode = loadSearchRequest.getMode();
        if(mode == null) {
            System.out.println("ERROR: mode was null");
            mode = "Region";
        }

        switch (mode) {
            case "Region":
                return searchByRegion(loadSearchRequest);
            case "Station":
                return stationToStationStrategy.search(loadSearchRequest);
            default:
                throw new RuntimeException("Unknown mode of " + loadSearchRequest.getMode());
        }
    }

    public ShipmentSearchResponse searchByRegion(LoadSearchRequest loadSearchRequest) {

        Job.getProgressMonitor().addExpectedWorkUnits(70);
        Job.getProgressMonitor().addExpectedWorkUnits(findRouteCount); // for routes below

        OrderStrategy orderStrategy = getOrderStrategy(loadSearchRequest.getStrategy());
        if(orderStrategy.daysToSellAvailable()) {
            Job.getProgressMonitor().addExpectedWorkUnits(findRouteCount); // for calculating daily quantity
        }


        // Get all of the Sell orders from the "fromRegion"
        // Get all of the Buy orders from the "toRegion"
        // Group orders by Product Type
        // Sort within each group from lowest to highest (sell orders) and highest to lowest (Buy orders)
        System.out.println("Get sell orders from source");
        OrderMap sellOrderMap = orderStrategy.findSourceOrderMap(loadSearchRequest);
        Job.getProgressMonitor().completeWorkUnits(10);

        System.out.println("Get buy orders from target");
        OrderMap buyOrderMap = orderStrategy.findTargetOrderMap(loadSearchRequest);
        Job.getProgressMonitor().completeWorkUnits(10);

        // For each product type calculate a shipment
        System.out.println("Find shipments");
        List<Shipment> shipments = findShipments(orderStrategy, loadSearchRequest, sellOrderMap, buyOrderMap);
        Job.getProgressMonitor().completeWorkUnits(10);

        System.out.println("Sort shipments");
        sortShipmentsByTotalProfit(shipments);
        Job.getProgressMonitor().completeWorkUnits(10);

        System.out.println("Calculate days to sell");
        calculateDaysToSell(orderStrategy, shipments);
        Job.getProgressMonitor().completeWorkUnits(10);

        System.out.println("Populate names");
        populateNames(shipments);
        Job.getProgressMonitor().completeWorkUnits(10);

        System.out.println("Find Routes");
        findRoutes(shipments);
        Job.getProgressMonitor().completeWorkUnits(10);

        System.out.println("Search finished");

        loadSearchRequest.setDaysToSellAvailable(orderStrategy.daysToSellAvailable());

        return new ShipmentSearchResponse(loadSearchRequest, shipments);
    }

    private OrderStrategy getOrderStrategy(String sellingStrategy) {
        OrderStrategy orderStrategy;
        switch (sellingStrategy) {
            case "SellToBuyOrder":
                orderStrategy = applicationContext.getBean(SellToBuyOrderStrategy.class);
                break;
            case "SellToSellOrder":
                orderStrategy = applicationContext.getBean(SellToSellOrderStrategy.class);
                break;
            case "SellToHub":
                orderStrategy = applicationContext.getBean(SellToHubStrategy.class);
                break;
            default:
                throw new RuntimeException("Unknown sellingStrategy: " + sellingStrategy);
        }
        return orderStrategy;
    }


    public List<Shipment> findShipments(OrderStrategy orderStrategy, LoadSearchRequest loadSearchRequest, OrderMap sellOrderMap, OrderMap buyOrderMap) {
        List<Shipment> shipments = new ArrayList<>();

        // Hmm - only looking at the One system with the cheapest of each ProductType (could perhaps go for 1-N of them to expand chances of finding enough volume)
        for(Integer productId : sellOrderMap.getProductOrderMap().keySet()) {
            ProductOrderList sellOrderList = sellOrderMap.getProductOrderMap().get(productId);
            ProductOrderList buyOrderList = buyOrderMap.getProductOrderMap().get(productId);

            boolean itemsForSale = sellOrderList != null && sellOrderList.getList().size() > 0;
            boolean ordersToBuy = buyOrderList != null && buyOrderList.getList().size() > 0;
            if(itemsForSale && ordersToBuy) {
                Shipment shipment = createShipmentForOneProduct(
                        orderStrategy, loadSearchRequest.getShipCapacity(), loadSearchRequest.getIskLimit(),
                        productId, sellOrderList, buyOrderList
                );

                if(shipment != null && shipment.getUnitLoadList().size() > 0 && shipment.getUnitSaleList().size() > 0) {
                    shipments.add(shipment);
                }
            }
        }

        return shipments;
    }

    private Shipment createShipmentForOneProduct(OrderStrategy orderStrategy, float shipCapacityM3, double iskLimit, Integer productId,
                                                 ProductOrderList sellOrderList, ProductOrderList buyOrderList) {
        Shipment shipment = new Shipment();

        float accumulatedCapacityM3 = 0;
        double accumulatedPurchaseCost = 0;
        double accumulatedSaleTotal = 0;

        Queue<MarketOrdersResponse> sellOrderQueue = new LinkedList<>(sellOrderList.getList());
        Queue<MarketOrdersResponse> buyOrderQueue = new LinkedList<>(buyOrderList.getList());

        UnitLoad unitLoad = null;
        UnitSale unitSale = null;

        MarketOrdersResponse sellOrder = null;
        MarketOrdersResponse buyOrder = null;

        int sellOrderQuantity = 0;
        int buyOrderQuantity = 0;

        boolean finished = false;
        while(!finished) {

            // are there any items left?
            boolean itemsAvailable = !sellOrderQueue.isEmpty() && !buyOrderQueue.isEmpty();

            // is there any capacity left?
            float remainingCapacityM3 = shipCapacityM3 - accumulatedCapacityM3;
            boolean capacityLeft = remainingCapacityM3 > 0;

            // are there any funds left?
            double remainingFunds = iskLimit - accumulatedPurchaseCost;
            boolean fundsLeft = remainingFunds > 0;

            // If there are items available, and we have the room and funds then attempt to buy some more stuff
            if(itemsAvailable && capacityLeft && fundsLeft) {

                if(sellOrder == null) {
                    sellOrder = sellOrderQueue.remove();
                    sellOrderQuantity = sellOrder.getVolumeRemain();
                }

                if(buyOrder == null) {
                    buyOrder = buyOrderQueue.remove();
                    buyOrderQuantity = buyOrder.getVolumeRemain();
                }

                Integer sellOrderLocation = sellOrder.getLocationId().intValue();
                double sellOrderPrice = sellOrder.getPrice();

                Integer buyOrderLocation = buyOrder.getLocationId().intValue();
                double buyOrderPrice = orderStrategy.getTargetOrderPrice(buyOrder);

                // If the purchase price is greater than the sale price then we're finished
                finished = sellOrderPrice >= buyOrderPrice;
                if(!finished) {

                    // we can only purchase the smaller of the two quantities
                    int purchaseQuantity = orderStrategy.getPurchaseOrderQuantity(sellOrderQuantity, buyOrderQuantity);

                    // how much of this purchase quantity can we fit into the ship?
                    purchaseQuantity = limitByVolume(remainingCapacityM3, purchaseQuantity, productId);

                    // how much of this purchase quantity can we buy with our remaining funds?
                    purchaseQuantity = limitByFunds(remainingFunds, purchaseQuantity, sellOrderPrice);

                    finished = purchaseQuantity < 1;
                    if(!finished) {

                        if(unitLoad == null || !(unitLoad.getFromLocationId().equals(sellOrderLocation) && unitLoad.getPurchasePrice().equals(sellOrderPrice))) {
                            unitLoad = new UnitLoad();
                            unitLoad.setFromLocationId(sellOrderLocation);
                            unitLoad.setProductId(productId);
                            unitLoad.setPurchasePrice(sellOrderPrice);
                            unitLoad.setAvailableQuantity(sellOrder.getVolumeRemain());
                            shipment.getUnitLoadList().add(unitLoad);
                        }
                        // else same location and same price, so we can just add to to the current unitLoad
                        unitLoad.setQuantity((unitLoad.getQuantity() != null ? unitLoad.getQuantity() : 0) + purchaseQuantity);


                        if(unitSale == null || !(unitSale.getToLocationId().equals(buyOrderLocation) && unitSale.getSalePrice().equals(buyOrderPrice))) {
                            unitSale = new UnitSale();
                            unitSale.setToLocationId(buyOrderLocation);
                            unitSale.setProductId(productId);
                            unitSale.setSalePrice(buyOrderPrice);
                            unitSale.setAvailableQuantity(buyOrder.getVolumeRemain());
                            shipment.getUnitSaleList().add(unitSale);
                        }
                        // else same location and same price, so we can just add to the current unitSale
                        unitSale.setSaleQuantity((unitSale.getSaleQuantity() != null ? unitSale.getSaleQuantity() : 0) + purchaseQuantity);


                        // accumulate the volume and funds for this purchase
                        accumulatedCapacityM3 += (purchaseQuantity * getProductVolume(productId));
                        accumulatedPurchaseCost += (purchaseQuantity * sellOrderPrice);
                        accumulatedSaleTotal += (purchaseQuantity * buyOrderPrice);

                        // decrement the quantities purchased
                        sellOrderQuantity -= purchaseQuantity;
                        buyOrderQuantity -= purchaseQuantity;

                        // and then if there nothing left from those orders set them to null so that on the next loop around we pick out the next in the queue
                        if(sellOrderQuantity < 1) {
                            sellOrder = null;
                        }

                        if(buyOrderQuantity < 1) {
                            buyOrder = null;
                        }
                    }
                }
            }
            else {
                finished = true;
            }
        }

        double tax = accumulatedSaleTotal * 0.02;

        shipment.setTotalPurchaseCost(accumulatedPurchaseCost);
        shipment.setTotalSaleAmount(accumulatedSaleTotal);
        shipment.setTotalProfit(accumulatedSaleTotal - (accumulatedPurchaseCost + tax));

        if(shipment.getTotalProfit() < 0) {
            shipment = null;
        }

        return shipment;
    }

    private Float getProductVolume(Integer productId) {
        Product product = productService.getProductMap().getMap().get(productId);
        if(product != null) {
            return product.getVolume();
        }
        else {
            System.out.println("Unable to find product for id = " + productId);
            return null;
        }
    }

    private int limitByVolume(float remainingCapacityM3, int purchaseQuantity, Integer productId) {
        Float productVolume = getProductVolume(productId);
        if(productVolume != null) {
            int howManyCouldWeTakeQty = Float.valueOf(remainingCapacityM3 / productVolume).intValue();
            int quantityLimitedByVolume = Math.min(howManyCouldWeTakeQty, purchaseQuantity);
            return quantityLimitedByVolume;
        }
        else {
            System.out.println("Unable to find product for id = " + productId);
            return 0;
        }
    }

    private int limitByFunds(double remainingFunds, int purchaseQuantity, double price) {
        int howManyCouldWeAfford = Double.valueOf(remainingFunds / price).intValue();
        int quantityLimitedByFunds = Math.min(howManyCouldWeAfford, purchaseQuantity);
        return quantityLimitedByFunds;
    }


    private void sortShipmentsByTotalProfit(List<Shipment> shipments) {
        shipments.sort((o1, o2) -> o2.getTotalProfit().compareTo(o1.getTotalProfit()));
    }

    private void calculateDaysToSell(OrderStrategy orderStrategy, List<Shipment> shipments) {
        for(int i = 0; i < Math.min(findRouteCount, shipments.size()); i++) {
            Shipment shipment = shipments.get(i);
            float maxDaysToSell = 0.0f;

            if(orderStrategy.daysToSellAvailable()) {

                Map<String, Float> dailyAverageQuantityCache = new HashMap<>();

                shipment.getUnitSaleList().forEach(unitSale -> {
                    Integer regionId = regionService.findRegionIdFromStationId(unitSale.getToLocationId());
                    Integer productId = unitSale.getProductId();

                    String key = regionId + "+" + productId;
                    Float dailyAverageQuantity = dailyAverageQuantityCache.get(key);
                    if(dailyAverageQuantity == null) {
                        dailyAverageQuantity = orderStrategy.getDailyAverageQuantity(regionId, productId);
                        dailyAverageQuantityCache.put(key, dailyAverageQuantity);
                    }

                    int quantity = unitSale.getSaleQuantity();
                    float daysToSell = ((float)quantity) / dailyAverageQuantity;
                    daysToSell = Math.min(daysToSell, 365);
                    System.out.println("daily average for " + productId + " = " + dailyAverageQuantity + ", daysToSell = " + daysToSell);

                    unitSale.setDaysToSell(daysToSell);
                });

                for (UnitSale unitSale : shipment.getUnitSaleList()) {
                    if (unitSale.getDaysToSell() != null && unitSale.getDaysToSell() > maxDaysToSell) {
                        maxDaysToSell = unitSale.getDaysToSell();
                    }
                }

                Job.getProgressMonitor().completeWorkUnits(1);
            }

            shipment.setMaxDaysToSell(maxDaysToSell);
        }
    }

    public void populateNames(List<Shipment> shipments) {
        Set<Integer> idSet = new HashSet<>();
        for(Shipment shipment : shipments) {
            for(UnitLoad unitLoad : shipment.getUnitLoadList()) {
                idSet.add(unitLoad.getFromLocationId());
            }

            for(UnitSale unitSale : shipment.getUnitSaleList()) {
                idSet.add(unitSale.getToLocationId());
            }
        }

        NameMap nameMap = nameCacheService.loadNames(new ArrayList(idSet));
        ProductMap productMap = productService.getProductMap();

        for(Shipment shipment : shipments) {
            for(UnitLoad unitLoad : shipment.getUnitLoadList()) {
                unitLoad.setProductName(productMap.getProductNameSafely(unitLoad.getProductId()));
                unitLoad.setFromLocationName(nameMap.getNameSafely(unitLoad.getFromLocationId()));
            }

            for(UnitSale unitSale : shipment.getUnitSaleList()) {
                unitSale.setProductName(productMap.getProductNameSafely(unitSale.getProductId()));
                unitSale.setToLocationName(nameMap.getNameSafely(unitSale.getToLocationId()));
            }
        }
    }


    private void findRoutes(List<Shipment> shipments) {
        // TODO: this is not the most efficient route that could be taken
        for (int i = 0; i < Math.min(findRouteCount, shipments.size()); i++) {
            Shipment shipment = shipments.get(i);

            System.out.println("Finding routes for shipment: " + i);

            List<Integer> fromLocationList = new ArrayList<>();
            shipment.getUnitLoadList().forEach(unitLoad -> {
                fromLocationList.add(unitLoad.getFromLocationId());
            });

            List<Integer> toLocationList = new ArrayList<>();
            shipment.getUnitSaleList().forEach(unitSale -> {
                toLocationList.add(unitSale.getToLocationId());
            });


            //if(between(fromLocationId, 0, 108445904) && between(toLocationId, 0, 108445904)) {

            List<SolarSystem> route = routeFinderService.findRoute(null, fromLocationList, toLocationList);
            shipment.setSystemRouteList(route);

            shipment.setNumberOfJumps(route.size());
            if(shipment.getNumberOfJumps() > 0) {
                shipment.setProfitPerJump(shipment.getTotalProfit() / shipment.getNumberOfJumps());
            }

            // Route count work units added right at start
            Job.getProgressMonitor().completeWorkUnits(1);
        }
    }

    public static boolean between(int value, int min, int max) {
        return value > min && value < max;
    }


}
