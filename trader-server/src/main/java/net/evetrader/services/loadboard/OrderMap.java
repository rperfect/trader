package net.evetrader.services.loadboard;

import java.util.HashMap;
import java.util.Map;

public class OrderMap {

    private Map<Integer, ProductOrderList> productOrderMap = new HashMap<>();


    public OrderMap() {
    }

    public Map<Integer, ProductOrderList> getProductOrderMap() {
        return productOrderMap;
    }

    public void setProductOrderMap(Map<Integer, ProductOrderList> productOrderMap) {
        this.productOrderMap = productOrderMap;
    }
}
