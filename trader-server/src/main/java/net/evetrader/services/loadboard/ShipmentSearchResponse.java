package net.evetrader.services.loadboard;

import net.evetrader.domain.Shipment;

import java.util.List;

public class ShipmentSearchResponse {

    private LoadSearchRequest loadSearchRequest;
    private List<Shipment> shipmentList;

    public ShipmentSearchResponse() {
    }

    public ShipmentSearchResponse(LoadSearchRequest loadSearchRequest, List<Shipment> shipmentList) {
        this.loadSearchRequest = loadSearchRequest;
        this.shipmentList = shipmentList;
    }

    public LoadSearchRequest getLoadSearchRequest() {
        return loadSearchRequest;
    }

    public void setLoadSearchRequest(LoadSearchRequest loadSearchRequest) {
        this.loadSearchRequest = loadSearchRequest;
    }

    public List<Shipment> getShipmentList() {
        return shipmentList;
    }

    public void setShipmentList(List<Shipment> shipmentList) {
        this.shipmentList = shipmentList;
    }
}
