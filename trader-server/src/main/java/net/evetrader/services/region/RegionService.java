package net.evetrader.services.region;

import net.evetrader.domain.UniverseName;
import net.evetrader.services.api.ApiCallable;
import net.evetrader.services.api.ApiService;
import net.evetrader.services.name.UniverseNameMapper;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.model.ConstellationResponse;
import net.troja.eve.esi.model.StationResponse;
import net.troja.eve.esi.model.SystemResponse;
import net.troja.eve.esi.model.UniverseNamesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class RegionService {

    @Autowired
    protected ApiService apiService;

    @Autowired
    protected UniverseNameMapper universeNameMapper;

    protected List<UniverseName> nameList;

    private Map<Integer, Integer> regionIdMapByStationId = new HashMap<>();
    private Map<Integer, Integer> regionIdMapBySystemId = new HashMap<>();

    public List<UniverseName> getRegionNames() {
        try {
            if(nameList == null) {
                List<Integer> universeRegions = apiService.getUniverseApi().getUniverseRegions(null, null);

                List<UniverseNamesResponse> universeNamesResponses = apiService.getUniverseApi().postUniverseNames(universeRegions, null);

                nameList = new ArrayList<>();
                for (UniverseNamesResponse nextNameResponse : universeNamesResponses) {
                    nameList.add(universeNameMapper.toUniverseName(nextNameResponse));
                }
            }

            return nameList;
        }
        catch (ApiException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<String, UniverseName> getRegionNameMapByName() {
        Map<String, UniverseName> map = new HashMap<>();
        List<UniverseName> regionNameList = getRegionNames();
        for(UniverseName nextName : regionNameList) {
            map.put(nextName.getName(), nextName);
        }
        return map;
    }

    public Integer findRegionIdFromStationId(Integer stationId) {
        try {
            Integer regionId = regionIdMapByStationId.get(stationId);
            if (regionId == null) {
                StationResponse stationResponse = new ApiCallable<StationResponse>() {
                    @Override
                    public StationResponse callApi() throws ApiException {
                        return apiService.getUniverseApi().getUniverseStationsStationId(stationId, null, null);
                    }
                }.call();

                // find the region
                Integer systemId = stationResponse.getSystemId();
                regionId = findRegionIdFromSystemId(systemId);
                regionIdMapByStationId.put(stationId, regionId);
            }

            return regionId;
        }
        catch (ApiException ex) {
            apiService.logApiException(ex);
            throw new RuntimeException(ex);
        }
    }

    public Integer findRegionIdFromSystemId(Integer systemId) {
        try {
            Integer regionId = regionIdMapBySystemId.get(regionIdMapBySystemId);
            if(regionId == null) {
                SystemResponse systemResponse = new ApiCallable<SystemResponse>() {
                    @Override
                    public SystemResponse callApi() throws ApiException {
                        return apiService.getUniverseApi().getUniverseSystemsSystemId(systemId, null, null, null, null);
                    }
                }.call();

                Integer constellationId = systemResponse.getConstellationId();
                ConstellationResponse constellationResponse = new ApiCallable<ConstellationResponse>() {
                    @Override
                    public ConstellationResponse callApi() throws ApiException {
                        return apiService.getUniverseApi().getUniverseConstellationsConstellationId(constellationId, null, null, null, null);
                    }
                }.call();

                regionId = constellationResponse.getRegionId();
                regionIdMapBySystemId.put(systemId, regionId);
            }
            return regionId;
        }
        catch (ApiException ex) {
            apiService.logApiException(ex);
            throw new RuntimeException(ex);
        }
    }

}
