package net.evetrader.services.region;

import net.evetrader.domain.UniverseName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/regions")
public class RegionController {

    @Autowired
    protected RegionService regionService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public List<UniverseName> getRegionNames() {
        return regionService.getRegionNames();
    }
}
