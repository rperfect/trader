package net.evetrader.services.hub;

import net.evetrader.domain.Station;
import net.evetrader.services.loadboard.OrderMap;
import net.evetrader.services.order.OrderMapFactory;
import net.evetrader.services.order.OrderService;
import net.evetrader.services.region.RegionService;
import net.troja.eve.esi.model.MarketOrdersResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class HubService {

    public static final String[][] HUBS = {
            {"60008494", "10000043", "Amarr VIII (Oris) - Emperor Family Academy"},
            /*{"60003760", "10000002", "Jita IV - Moon 4 - Caldari Navy Assembly Plant"},
            {"60011866", "10000032", "Dodixie IX - Moon 20 - Federation Navy Assembly Plant"},
            {"60004588", "10000030", "Rens VI - Moon 8 - Brutor Tribe Treasury"},
            {"60005686", "10000042", "Hek VIII - Moon 12 - Boundless Creation Factory"}*/
    };

    @Autowired
    protected OrderService orderService;

    @Autowired
    protected RegionService regionService;

    @Autowired
    protected OrderMapFactory orderMapFactory;


    private List<Station> hubStations;

    public List<Station> getHubStations() {
        if (hubStations == null) {
            hubStations = new ArrayList<>();

            for(String[] nextHub : HUBS) {
                Station station = new Station();
                station.setStationID(Integer.parseInt(nextHub[0]));
                station.setRegionID(Integer.parseInt(nextHub[1]));
                station.setStationName(nextHub[2]);
                hubStations.add(station);
            }
        }
        return hubStations;
    }

    public OrderMap findOrderMapForHubs(String orderType, OrderService.SortOrder sortOrder) {

        List<MarketOrdersResponse> allHubsOrderList = new ArrayList<>();
        List<Station> hubStationList = getHubStations();
        hubStationList.forEach(station -> {
            List<MarketOrdersResponse> orderList = findOrderList(station, orderType);
            allHubsOrderList.addAll(orderList);
        });

        OrderMap orderMap = orderMapFactory.create(allHubsOrderList, sortOrder);
        return orderMap;
    }

    public OrderMap findOrderMapForStation(Station station, String orderType, OrderService.SortOrder sortOrder) {
        List<MarketOrdersResponse> orderList = findOrderList(station, orderType);
        OrderMap orderMap = orderMapFactory.create(orderList, sortOrder);
        return orderMap;
    }

    private List<MarketOrdersResponse> findOrderList(Station station, String orderType) {
        Integer stationId = station.getStationID();
        Integer regionId = regionService.findRegionIdFromStationId(stationId);

        // find the sell orders for that region
        List<MarketOrdersResponse> orderList = orderService.getOrderList(orderType, regionId);

        // find the sell orders for the hub location
        List<MarketOrdersResponse> filteredList = new ArrayList<>();
        orderList.forEach(order -> {
            if(order.getLocationId().intValue() == stationId) {
                filteredList.add(order);
            }
        });

        return filteredList;
    }


    /*public static void main(String[] args) {
        EveTraderApplication.startDatabase();
        ApplicationContext applicationContext = SpringApplication.run(EveTraderApplication.class, args);

        HubService hubService = applicationContext.getBean(HubService.class);

        for(String[] nextHubData : HubService.HUBS) {
            Integer stationId = Integer.parseInt(nextHubData[0]);
            Integer regionId = hubService.regionService.findRegionIdFromStationId(stationId);
            System.out.println("Station to Region: " + stationId + ", " + regionId);
        }

    }*/
}
