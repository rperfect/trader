package net.evetrader.services.product;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class MarketGroupService {

    private MarketGroupMap marketGroupMap;

    public MarketGroupMap getMarketGroupMap() {
        if (marketGroupMap == null) {
            marketGroupMap = readMarketGroupMap();
        }
        return marketGroupMap;
    }

    private MarketGroupMap readMarketGroupMap() {
        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            TypeReference<List<MarketGroup>> typeRef = new TypeReference<>() {};

            List<MarketGroup> list = mapper.readValue(ProductService.class.getResourceAsStream("/invMarketGroups.yaml"), typeRef);

            MarketGroupMap marketGroupMap = new MarketGroupMap();
            for(MarketGroup marketGroup : list) {
                marketGroupMap.getMap().put(marketGroup.getMarketGroupID(), marketGroup);
            }

            return marketGroupMap;
        }
        catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }
}
