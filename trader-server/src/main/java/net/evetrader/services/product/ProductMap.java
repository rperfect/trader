package net.evetrader.services.product;

import net.evetrader.domain.Product;

import java.util.HashMap;
import java.util.Map;

public class ProductMap {

    private Map<Integer, Product> map = new HashMap<>();

    public ProductMap() {
    }

    public Map<Integer, Product> getMap() {
        return map;
    }

    public String getProductNameSafely(Integer productId) {
        Product product = map.get(productId);
        String productName = "UNKNOWN PRODUCT";
        if(product != null) {
            productName = product.getName() != null ? product.getName().getEn() : "UNKNOWN PRODUCT NAME";
        }
        return productName;
    }
}
