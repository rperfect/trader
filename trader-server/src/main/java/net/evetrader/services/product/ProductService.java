package net.evetrader.services.product;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import net.evetrader.domain.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ProductService {

    private ProductMap productMap;

    @Autowired
    protected MarketGroupService marketGroupService;

    public synchronized ProductMap getProductMap() {
        if(productMap == null) {
            productMap = readProductMap();
        }
        return productMap;
    }

    private ProductMap readProductMap() {
        try {
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            TypeReference<HashMap<Integer, Product>> typeRef = new TypeReference<>() {};

            MarketGroupMap marketGroupMap = marketGroupService.getMarketGroupMap();

            Map<Integer, Product> productMap = mapper.readValue(ProductService.class.getResourceAsStream("/typeIDs.yaml"), typeRef);

            ProductMap newProductMap = new ProductMap();
            for(Integer nextKey : productMap.keySet()) {
                Product product = productMap.get(nextKey);
                product.setProductId(nextKey);

                String marketGroupName = marketGroupMap.getFullyQualifedName(product.getMarketGroupID());
                product.setMarketGroupName(marketGroupName);

                newProductMap.getMap().put(nextKey, product);
            }

            return newProductMap;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
