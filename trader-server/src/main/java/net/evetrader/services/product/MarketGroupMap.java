package net.evetrader.services.product;

import java.util.HashMap;
import java.util.Map;

public class MarketGroupMap {

    private Map<Integer, MarketGroup> map = new HashMap<>();

    public MarketGroupMap() {
    }

    public Map<Integer, MarketGroup> getMap() {
        return map;
    }

    public String getParentGroupName(Integer marketGroupID) {
        MarketGroup marketGroup = getMap().get(marketGroupID);
        if(marketGroup != null) {
            if(marketGroup.getParentGroupID() != null) {
                return getParentGroupName(marketGroup.getParentGroupID());
            }
            else {
                return marketGroup.getMarketGroupName();
            }
        }
        else {
            return "UNKNOWN PARENT GROUP - " + marketGroupID;
        }
    }

    public String getFullyQualifedName(Integer marketGroupID) {
        String name;
        MarketGroup marketGroup = getMap().get(marketGroupID);
        if(marketGroup != null) {
            StringBuilder bob = new StringBuilder();
            getNameRecursively(marketGroup, bob);
            name = bob.toString();
        }
        else {
            name = "UNKNOWN MARKET GROUP - " + marketGroupID;
        }
        return name;
    }

    private void getNameRecursively(MarketGroup marketGroup, StringBuilder builder) {
        if(marketGroup.getParentGroupID() != null) {
            getNameRecursively(getMap().get(marketGroup.getParentGroupID()), builder);
            builder.append(", ");
        }

        builder.append(marketGroup.getMarketGroupName());
    }
}
