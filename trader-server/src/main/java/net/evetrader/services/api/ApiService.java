package net.evetrader.services.api;

import net.evetrader.services.authentication.MySecurityContext;
import net.evetrader.services.authentication.MySecurityContextHolder;
import net.troja.eve.esi.ApiClient;
import net.troja.eve.esi.ApiClientBuilder;
import net.troja.eve.esi.ApiException;
import net.troja.eve.esi.api.MarketApi;
import net.troja.eve.esi.api.RoutesApi;
import net.troja.eve.esi.api.UniverseApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Component
public class ApiService {

    //public static final String clientId = "ef2782230f69486c92dedf7b5527f66c";
    //public static final String REFRESH_TOKEN = "_gtSXtpVJTox6EWJ9neFPKQsaa4atQfoBA2t4BdXz2E";

    //public static final String clientId = "c70f987c21804b79b675303efb3e7d4a";
    //public static final String REFRESH_TOKEN = "rMG0MAkeu02okFZxq40stg==";

    @Value("${application.oauth.clientId:c70f987c21804b79b675303efb3e7d4a}")
    public String clientId;

    private ApiClient client;
    private UniverseApi universeApi;
    private MarketApi marketApi;
    private RoutesApi routesApi;


    public ApiService() {
    }

    public ApiClient getClient() {
        if (client == null) {
            MySecurityContext securityContext = MySecurityContextHolder.get();

            if(securityContext != null && securityContext.getMyUserDetails() != null) {
                String refreshToken = securityContext.getMyUserDetails().getRefreshToken();
                client = new ApiClientBuilder().clientID(clientId).refreshToken(refreshToken).build();
            }
            else {
                client = new ApiClientBuilder().clientID(clientId).build();
            }

            client.getHttpClient().setConnectTimeout(20, TimeUnit.SECONDS);
            client.getHttpClient().setReadTimeout(20, TimeUnit.SECONDS);
            client.getHttpClient().setWriteTimeout(20, TimeUnit.SECONDS);
        }
        return client;
    }

    public UniverseApi getUniverseApi() {
        if (universeApi == null) {
            universeApi = new UniverseApi(getClient());
        }
        return universeApi;
    }

    public MarketApi getMarketApi() {
        if (marketApi == null) {
            marketApi = new MarketApi(getClient());
        }
        return marketApi;
    }

    public RoutesApi getRoutesApi() {
        if (routesApi == null) {
            routesApi = new RoutesApi(getClient());
        }
        return routesApi;
    }

    public void logApiException(ApiException ex) {
        System.out.println(formatApiExcetion(ex));
    }

    public String formatApiExcetion(ApiException ex) {
        StringBuilder bob = new StringBuilder();
        bob.append("ApiException: ").append(ex.getClass().getName()).append(" ").append(ex.getCode()).append(" ").append(ex.getMessage()).append("\n");
        if(ex.getResponseHeaders() != null) {
            for (String nextHeaderKey : ex.getResponseHeaders().keySet()) {
                List<String> headerList = ex.getResponseHeaders().get(nextHeaderKey);
                for (String nextHeaderValue : headerList) {
                    bob.append(nextHeaderKey).append(": ").append(nextHeaderValue).append("\n");
                }
            }
        }
        bob.append("\n");
        bob.append(ex.getResponseBody()).append("\n");

        return bob.toString();
    }



}
