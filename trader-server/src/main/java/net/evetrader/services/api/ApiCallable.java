package net.evetrader.services.api;

import net.troja.eve.esi.ApiException;

import java.util.concurrent.Callable;

public abstract class ApiCallable<T> implements Callable<T> {

    @Override
    public T call() throws ApiException {
        int retryCount = 0;
        final int MAX_RETRIES = 5;
        ApiException lastException = null;
        while(retryCount++ < MAX_RETRIES) {
            try {
                return callApi();
            }
            catch (ApiException ex) {
                if(ex.getCode() >= 500) {
                    lastException = ex;
                    System.out.println("ApiException[" + retryCount + "]: " + ex.getCode() + " " + ex.getMessage());
                    try { Thread.sleep(retryCount * 1000); } catch (InterruptedException e2) {}
                }
                else {
                    throw ex;
                }
            }
        }

        throw lastException;
    }

    abstract public T callApi() throws ApiException;
}
