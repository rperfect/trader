import {UnitLoad} from "./unit-load";
import {UnitSale} from "./unit-sale";
import {SaleStrategy} from "./sale-strategy";

export class Shipment {

  unitLoadList: UnitLoad[];
  unitSaleList: UnitSale[];

  sellOrderSaleStrategy: SaleStrategy;

  totalPurchaseCost: number;
  totalSaleAmount: number;
  totalProfit: number;
  numberOfJumps: number;
  profitPerJump: number;
  pickupJumpNumber: number;
  maxDaysToSell: number;

  systemRouteList: any[];

  constructor() {}

}
