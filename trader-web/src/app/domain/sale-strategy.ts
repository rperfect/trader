import {UnitSale} from "./unit-sale";

export class SaleStrategy {

  unitSaleList: UnitSale[];

  totalSaleAmount: number;
  totalProfit: number;
  profitPerJump: number;

  constructor(){}
}
