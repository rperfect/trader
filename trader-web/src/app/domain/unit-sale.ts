export class UnitSale {

  productId: number;
  productName: string;
  toLocationId: number;
  toLocationName: string;
  saleQuantity: number;
  availableQuantity: number;
  salePrice: number;
  daysToSell: number;

  constructor() {}

}
