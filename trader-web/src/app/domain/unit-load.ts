import {UnitSale} from "./unit-sale";

export class UnitLoad {

  productId: number;
  productName: string;
  fromLocationId: number;
  fromLocationName: string;
  quantity: number;
  availableQuantity: number;
  purchasePrice: number;

  constructor() {}

}
