import {LoadSearchRequest} from "./load-search-request";
import {Shipment} from "../../domain/shipment";

export class ShipmentSearchResponse {

  public loadSearchRequest: LoadSearchRequest;
  public shipmentList: Shipment[];

  constructor() {}
}
