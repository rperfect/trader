import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {LoadSearchRequest} from "./load-search-request";

@Injectable({
  providedIn: 'root'
})
export class LoadBoardService {

  constructor(private http: HttpClient) { }

  public search(loadSearchRequest: LoadSearchRequest, jobId: string) : Observable<any> {
    return this.http.post<any>('/api/load-board/search', loadSearchRequest, { params: new HttpParams().set('jobId', jobId) });
  }
}
