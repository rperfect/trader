import {UniverseName} from "../../domain/universe-name";

export class LoadSearchRequest {

  mode : string = null;
  strategy = "SellToHub";  // SellToBuyOrder, SellToSellOrder, SellToHub

  fromLocationId = 60008494;
  fromLocationName = 'Amarr VIII (Oris) - Emperor Family Academy';

  fromRegionName = "Domain";
  fromRegion = new UniverseName(10000043, "Domain", "REGION");
  shipCapacity = 13800;
  iskLimit = 40000000;

  toLocationId = 60001822;
  toLocationName = 'Kulelen V - Moon 8 - Zainou Biotech Production';

  toRegionName = "The Citadel";
  toRegion = new UniverseName(10000033, "The Citadel", "REGION" );

  daysToSellAvailable: boolean = false;
}
