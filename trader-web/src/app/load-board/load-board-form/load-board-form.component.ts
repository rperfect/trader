import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LoadSearchRequest} from "../service/load-search-request";
import {UniverseName} from "../../domain/universe-name";
import {RegionService} from "../../region/region.service";
import {TypeaheadMatch} from "ngx-bootstrap";
import {Station} from "../../domain/station";
import {StationService} from "../../station/station.service";

@Component({
  selector: 'app-load-board-form',
  templateUrl: './load-board-form.component.html'
})
export class LoadBoardFormComponent implements OnInit {

  loadSearchRequest = new LoadSearchRequest();

  stations: Station[];
  regionNames: UniverseName[];

  strategyOptions = ['Single', 'Multiple'];
  sellingStrategyOptions = ['SellToSellOrder', 'SellToBuyOrder', 'SellToHub'];

  @Input()
  mode;

  @Output()
  searchAction = new EventEmitter();

  constructor(private regionService: RegionService,
              private stationService: StationService) { }

  ngOnInit() {
    this.regionService.getRegionNames().subscribe((regionNames) => this.regionNames = regionNames);
    this.stationService.getStations().subscribe((stationsList) => {
      this.stations = stationsList;
      console.log('Stations loaded: ' + this.stations.length);
    });

    this.loadSearchRequest.mode = this.mode;
  }

  onSearch() {
    console.log('onSearch()');
    this.searchAction.next(this.loadSearchRequest);
  }

  onReset() {
    this.loadSearchRequest = new LoadSearchRequest();
  }

  onFromRegionSelected(event: TypeaheadMatch) {
    this.loadSearchRequest.fromRegion = event.item;
  }

  onToRegionSelected(event: TypeaheadMatch) {
    this.loadSearchRequest.toRegion = event.item;
  }

  onFromLocationSelected(event: TypeaheadMatch) {
    this.loadSearchRequest.fromLocationId = event.item.stationID;
  }

  onToLocationSelected(event: TypeaheadMatch) {
    this.loadSearchRequest.toLocationId = event.item.stationID;
  }
}
