import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Shipment} from "../../domain/shipment";
import {ShipmentSearchResponse} from "../service/shipment-search-response";

@Component({
  selector: 'app-load-board-results',
  templateUrl: './load-board-results.component.html',
  styleUrls: ['load-board-results.component.css']
})
export class LoadBoardResultsComponent implements OnInit {

  @Input()
  shipmentSearchResponse: ShipmentSearchResponse;

  @Output()
  shipmentSelected = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onShipmentSelected(shipment: Shipment) {
    this.shipmentSelected.next(shipment);
  }
}
