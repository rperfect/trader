import {Component, OnInit, ViewChild} from '@angular/core';
import {LoadBoardService} from "./service/load-board.service";
import {LoadSearchRequest} from "./service/load-search-request";
import {Shipment} from "../domain/shipment";
import {ShipmentDetailsComponent} from "./shipment-details/shipment-details.component";
import {JobService} from "../job/job.service";
import {ShipmentSearchResponse} from "./service/shipment-search-response";
import {ActivatedRoute} from "@angular/router";

import { v4 as uuid } from 'uuid';

import {interval} from "rxjs";
import {startWith, switchMap} from "rxjs/operators";

@Component({
  selector: 'app-load-board',
  templateUrl: './load-board.component.html'
})
export class LoadBoardComponent implements OnInit {

  private _shipmentSearchResponse: ShipmentSearchResponse = null;

  @ViewChild('shipmentDetailsComponent')
  shipmentDetailsComponent: ShipmentDetailsComponent;

  mode : string = null;
  searchPercentComplete: number = null;

  private pollingSubscription: any;

  constructor(private loadBoardService: LoadBoardService,
              private jobService: JobService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.loadShipmentSearchResponse();
    this.route.queryParams.subscribe(params => {
      this.mode = params.mode;
    });
  }

  private loadShipmentSearchResponse() {
    if(this._shipmentSearchResponse === null) {
      this._shipmentSearchResponse = JSON.parse(localStorage.getItem('shipmentSearchResponse'));
    }
  }

  private saveShipmentSearchResponse() {
    localStorage.setItem('shipmentSearchResponse', JSON.stringify(this._shipmentSearchResponse));
  }

  get shipmentSearchResponse(): ShipmentSearchResponse {
    this.loadShipmentSearchResponse();
    return this._shipmentSearchResponse;
  }

  set shipmentSearchResponse(value: ShipmentSearchResponse) {
    this._shipmentSearchResponse = value;
    this.saveShipmentSearchResponse();
  }

  onSearch(loadSearchRequest: LoadSearchRequest) {
    console.log('LoadBoardComponent.onSearch()');

    this.shipmentSearchResponse = null;
    this.searchPercentComplete = 0;
    let jobId = uuid();
    this.loadBoardService.search(loadSearchRequest, jobId).subscribe((jobResponse) => {
      console.log('search complete');
      this.shipmentSearchResponse = jobResponse.result;
      this.pollingSubscription.unsubscribe();
    });

    this.monitorSearchProgress(jobId);
  }

  monitorSearchProgress(jobId: string) {
    console.log('search initiated for job ' + jobId + '. Monitoring...');

    this.pollingSubscription = interval(3000).pipe(
        startWith(0),
        switchMap(() => this.jobService.monitorJob(jobId))
      ).subscribe((monitorResponse) => {
        if(monitorResponse) {
          this.searchPercentComplete = monitorResponse.percentComplete;
        }
        console.log('Percent complete = ' + this.searchPercentComplete);
      });
  }

  onDisplayShipmentDetails() {
    this.shipmentDetailsComponent.showShipmentDetails(null);
  }

  onShipmentSelected(shipment: Shipment) {
    this.shipmentDetailsComponent.showShipmentDetails(shipment);
  }
}
