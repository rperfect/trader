import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-route-details',
  templateUrl: './route-details.component.html',
  styleUrls: ['route-details.component.css']
})
export class RouteDetailsComponent implements OnInit {

  @Input()
  systemRouteList: any[];

  securityStatusColor = [
    '#F00000',
    '#D73000',
    '#F04800',
    '#F06000',
    '#D77700',
    '#EFEF00',
    '#8FEF2F',
    '#00F000',
    '#00EF47',
    '#48F0C0',
    '#2FEFEF',
  ];

  constructor() { }

  ngOnInit() {
  }

  getSecurityStatusColor(securityStatus: number) {
    let colorIdx = Math.round(securityStatus * 10);
    if(colorIdx < 0) {
      colorIdx = 0;
    }

    if(colorIdx >= this.securityStatusColor.length) {
      colorIdx = this.securityStatusColor.length - 1;
    }

    return this.securityStatusColor[colorIdx];
  }

}
