import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Shipment} from "../../domain/shipment";
import {ModalDirective} from "ngx-bootstrap";
import {ShipmentSearchResponse} from "../service/shipment-search-response";

@Component({
  selector: 'app-shipment-details',
  templateUrl: './shipment-details.component.html'
})
export class ShipmentDetailsComponent implements OnInit {

  @ViewChild('shipmentDetailsModal')
  shipmentDetailsModal: ModalDirective;

  shipment: Shipment;

  @Input()
  shipmentSearchResponse: ShipmentSearchResponse;

  constructor() { }

  ngOnInit() {
  }

  showShipmentDetails(shipment: Shipment) {
    this.shipment = shipment;
    this.shipmentDetailsModal.show();
  }

  onClose() {
    this.shipmentDetailsModal.hide();
  }
}
