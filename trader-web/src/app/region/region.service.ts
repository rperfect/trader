import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RegionService {

  constructor(protected http: HttpClient) { }

  public getRegionNames() : Observable<any> {
    return this.http.get('/api/regions/');
  }
}
