import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class JobService {

  constructor(private http: HttpClient) { }

  public monitorJob(jobId: string): Observable<any> {
    return this.http.get('/api/job/monitor/' + jobId);
  }
}
