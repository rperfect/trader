import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {AuthenticationService} from "../authentication/authentication.service";

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html'
})
export class CallbackComponent implements OnInit {

  constructor(private router: Router,
              private route: ActivatedRoute,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params: Params) => {
      let jwtToken = params['jwtToken'];
      console.log('Got token: ' + jwtToken);
      this.router.navigate(['/load-board']);
      this.authenticationService.receiveCallback(jwtToken);
    })
  }

}
