import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";
import {HomePageComponent} from "./home-page/home-page.component";
import {LoadBoardComponent} from "./load-board/load-board.component";
import {CallbackComponent} from "./callback/callback.component";
import {AnalysisViewComponent} from "./analysis-view/analysis-view.component";

const routes: Routes = [

  { path: 'home', component: HomePageComponent},
  { path: 'load-board', component: LoadBoardComponent},
  { path: 'analysis', component: AnalysisViewComponent},
  { path: 'callback', component: CallbackComponent},

  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
