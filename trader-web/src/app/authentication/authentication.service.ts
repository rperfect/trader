import {EventEmitter, Injectable, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, Observable} from "rxjs";
import {JwtHelperService} from "@auth0/angular-jwt";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  public currentUser$ = new EventEmitter(true);

  constructor(private http: HttpClient,
              private jwtHelperService: JwtHelperService) { }


  public getAuthorizationUri() : Observable<any> {
    return this.http.get('/api/oauth/authorizationUri');
  }

  public receiveCallback(token: string) {
    localStorage.setItem('currentUser', token);
    this.setUserFromToken(token);
  }

  public setUserFromToken(token: string) {
    let decodedToken = this.jwtHelperService.decodeToken(token);

    let user = new User();
    user.characterId = decodedToken.sub;
    user.expiryTime = decodedToken.exp;
    user.name = decodedToken.name;

    this.currentUser$.next(user);
  }

  public logout() {
    localStorage.removeItem('currentUser');
    this.currentUser$.next(null);
  }
}
