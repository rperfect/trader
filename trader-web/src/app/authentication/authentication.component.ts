import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "./authentication.service";

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html'
})
export class AuthenticationComponent implements OnInit {

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

  onClick() {
    this.authenticationService.getAuthorizationUri().subscribe((response) => {
      window.location.href = response.uri;
    })
  }
}
