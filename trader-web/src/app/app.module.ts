import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {JwtModule} from "@auth0/angular-jwt";
import {JwtHelperService} from "@auth0/angular-jwt";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { LoadBoardComponent } from './load-board/load-board.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import {BsDropdownModule, ModalModule, ProgressbarModule, TooltipModule, TypeaheadModule} from "ngx-bootstrap";
import { LoadBoardFormComponent } from './load-board/load-board-form/load-board-form.component';
import { LoadBoardResultsComponent } from './load-board/load-board-results/load-board-results.component';
import { ShipmentDetailsComponent } from './load-board/shipment-details/shipment-details.component';
import { AuthenticationComponent } from './authentication/authentication.component';
import { CallbackComponent } from './callback/callback.component';
import { AnalysisViewComponent } from './analysis-view/analysis-view.component';
import { RouteDetailsComponent } from './load-board/shipment-details/route-details/route-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    LoadBoardComponent,
    PageNotFoundComponent,
    LoadBoardFormComponent,
    LoadBoardResultsComponent,
    ShipmentDetailsComponent,
    AuthenticationComponent,
    CallbackComponent,
    AnalysisViewComponent,
    RouteDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,

    BsDropdownModule.forRoot(),
    TypeaheadModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TooltipModule.forRoot(),

    JwtModule.forRoot({
      config: {
        tokenGetter: myTokenGetter
      }
    }),

  ],
  providers: [
    JwtHelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



export function myTokenGetter() {
  let currentUser = localStorage.getItem('currentUser');
  return currentUser ? currentUser : '';
}
