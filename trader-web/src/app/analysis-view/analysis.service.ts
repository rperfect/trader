import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AnalysisService {

  constructor(private http: HttpClient) { }

  public regionToRegionAnalysis() : Observable<any> {
    return this.http.post<any>('/api/analysis/regionToRegion', null);
  }
}
