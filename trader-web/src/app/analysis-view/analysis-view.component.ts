import { Component, OnInit } from '@angular/core';
import {AnalysisService} from "./analysis.service";
import {JobService} from "../job/job.service";

@Component({
  selector: 'app-analysis-view',
  templateUrl: './analysis-view.component.html'
})
export class AnalysisViewComponent implements OnInit {

  jobComplete: number = null;

  constructor(private analysisService: AnalysisService,
              private jobService: JobService) { }

  ngOnInit() {
  }

  onStartJob() {
    this.jobComplete = 0;
    this.analysisService.regionToRegionAnalysis().subscribe((firstResponse) => {
      let jobId = firstResponse.jobId;
      console.log('Analysis initiated for job ' + jobId + '. Monitoring...');
      this.monitorSearchProgress(jobId);
    });
  }

  monitorSearchProgress(jobId: string) {
    this.jobService.monitorJob(jobId).subscribe((monitorResponse) => {
      this.jobComplete = monitorResponse.percentComplete;
      if(monitorResponse.finished) {
        this.jobComplete = null;
      }
      else {
        this.monitorSearchProgress(jobId);
      }
    });
  }

}
