import {Component, OnInit} from '@angular/core';
import {User} from "./authentication/user";
import {AuthenticationService} from "./authentication/authentication.service";
import {Router} from "@angular/router";
import {JwtHelperService} from "@auth0/angular-jwt";
import * as moment from "moment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  currentUser: User;

  constructor(private router: Router,
              private authenticationService: AuthenticationService,
              private jwtHelperService: JwtHelperService) {}

  ngOnInit(): void {
    this.authenticationService.currentUser$.subscribe((user) => {
      this.currentUser = user;
    });

    this.loadCurrentUser();
  }

  loadCurrentUser(): void {
    let currentUserToken = localStorage.getItem('currentUser');
    if(currentUserToken) {
      let decodedToken = this.jwtHelperService.decodeToken(currentUserToken);
      let expiry = moment(decodedToken.exp * 1000);
      if(expiry.isAfter(moment())) {
        console.log('AppComponent.starting: Got valid user [' + currentUserToken + ']');
        this.authenticationService.receiveCallback(currentUserToken);
      }
      else {
        console.log('AppComponent.starting: expired token: ' + expiry);
        localStorage.removeItem('currentUser');
        this.router.navigate(['/home']);
      }
    }
    else {
      console.log('AppComponent.starting: No user found');
    }
  }

  isAuthenticated() : boolean {
    return this.currentUser != null;
  }

  onLogout() {
    this.authenticationService.logout();
    this.router.navigate(['/home']);
  }
}
